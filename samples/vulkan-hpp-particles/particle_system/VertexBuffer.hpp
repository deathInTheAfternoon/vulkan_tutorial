//
// Created by navee on 30/07/2021.
//

#ifndef VULKAN_TEST_VERTEXBUFFER_HPP
#define VULKAN_TEST_VERTEXBUFFER_HPP

// For easy unmarshalling to C like arrays and to ensure sizeof works correctly.
struct Vertex {
    float x;
    float y;
    float z;
    float w{1.0f};
};

// Todo: You could add primitive restart symbol after each trail?
struct GiantBuffer {
    void initialize(int particleCount = 1, int trailLength = 2) {
        numberOfParticles = particleCount;
        this->trailLength = trailLength;

        trail.resize(numberOfParticles * trailLength);
        orderedArray.resize(numberOfParticles * trailLength);
        iWriteSlots.resize(numberOfParticles);
    }

    // Read a vertex. We offset from the current write slot as everything after is the start of the old array.
    Vertex& read(int trailNumber, int iVertex) {
        int startOfThisTrail = trailNumber * trailLength;
        int actualIndex = iWriteSlots[trailNumber] + iVertex;
        actualIndex = startOfThisTrail + (actualIndex % trailLength);
        return trail[actualIndex];
    }

    // You can ONLY write to the current slot.
    void write(int trailNumber, const Vertex& input) {
        int startOfThisTrail = trailNumber * trailLength;
        int actualIndex = startOfThisTrail + iWriteSlots[trailNumber];
        Vertex& v = trail[actualIndex];
        v.x = input.x;
        v.y = input.y;
        v.z = input.z;
        iWriteSlots[trailNumber] = (iWriteSlots[trailNumber] + 1) % trailLength;
    }

    std::vector<Vertex> &getOrderedArray() {
        int pos = 0;
        for (int i = 0; i < numberOfParticles; i++) {
            for (int j = 0; j < trailLength; j++, pos++)
                orderedArray[pos] = read(i, j);

        }
        return orderedArray;
    }


    // Fill preAllocatedVector starting (including) offset.
    void fillWithOrderedArray(std::vector<Vertex>& preAllocatedVector, int offset) {
        int pos = offset;
        for (int i = 0; i < numberOfParticles; i++) {
            for (int j = 0; j < trailLength; j++, pos++)
                preAllocatedVector[pos] = read(i, j);

        }
    }

    // The number of particles
    size_t numberOfParticles;
    std::vector<int> iWriteSlots;
    size_t trailLength;
    std::vector<Vertex> trail;
    std::vector<Vertex> orderedArray;
};

#endif //VULKAN_TEST_VERTEXBUFFER_HPP
