//
// Created by navee on 02/07/2021.
//

#ifndef VULKAN_TEST_DebugTriangle_H
#define VULKAN_TEST_DebugTriangle_H


#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "glad/glad.h"

class DebugTriangle {
public:
    DebugTriangle() { initBuffers(); }
    void initBuffers();
    std::vector<glm::vec3> getLocalCoordsOfVertices();
    glm::mat4 getLocalTransformations();
    int getVertexCount();
    float* getFloatArray() { return nullptr; }
    void draw();

    // Positioning methods
    // Local point around which object rotates and scale. Also point used by world to place object.
    void setPointOfTransformation(glm::vec3 point);
    // Overwrite current location
    void setWorldLocation(glm::vec3 newPos);
    glm::vec3 getWorldLocation();
    void incrementOrientation(float radians);
    // This does not affect the PoT. You have to reset the PoT if you want to change the scale and
    // maintain the point of rotation. Naturally, the shape has changed size so this seems sensible.
    void setScale(glm::vec3 newScale);

    glm::vec3  getLocation();

    // Offset current location
    void deltaWorldLocation(glm::vec3 deltas);

    ~DebugTriangle() {
        glDeleteVertexArrays(1, &hVAO);
        glDeleteBuffers(1, &hVBO);
    }

    virtual GLuint getPrimitiveType() { return GL_TRIANGLE_FAN; }


private:
    GLuint hVAO;
    GLuint hVBO;
    // Position of local-PoT in 3D world
    glm::vec3 worldLocation = glm::vec3(0.f, 0.f, 0.f ); //world coords
    glm::vec3 pointOfTransformation = glm::vec3(0.f, 0.f, 0.f); //local coords.
    float orientation= 0.f;
    glm::vec3 scale = glm::vec3(1.f, 1.f, 1.f);

    std::vector<glm::vec4> vertices{ glm::vec4(-0.f, 0.0f, 0.f, 1.f),
                                     glm::vec4(0.5f, 1.f, 0.f, 1.f),
                                     glm::vec4(1.0f, 0.0f, 0.f, 1.f) };


};


#endif //VULKAN_TEST_DebugTriangle_H
