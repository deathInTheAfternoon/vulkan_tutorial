//
// Created by navee on 28/06/2021.
//

#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "Shape.h"

// Return handle to new vertex buffer object.
void Shape::initBuffers() {
    // Allocate one handle for a new hVAO (list of lists) and capture the handle.
    glCreateVertexArrays(1, &hVAO);
    // bind the list of lists so we can set its attributes during this call.
    glBindVertexArray(hVAO);

    // Allocate handle. todo: who will delete underlying objects?
    glGenBuffers(1, &hVBO);
    // Set the global state table's GL_ARRAY_BUFFER entry so following calls apply to this handle.
    glBindBuffer(GL_ARRAY_BUFFER, hVBO);

    // Allocate memory and copy in 'vertices' data. TODO: some examples currently use float*...hence this temporary decision
    const GLfloat* f_array = getFloatArray();
    if (f_array) {
        glBufferData(GL_ARRAY_BUFFER, 3 * getVertexCount() * sizeof(float), f_array, GL_STATIC_DRAW);
    } else {
        const std::vector<glm::vec3>& local_vertices = getLocalCoordsOfVertices();
        // We must calculate size of vector's contiguous memory in bytes.
        // vector.data(): elements in the vector are guaranteed to be stored in contiguous storage locations
        // in the same order as represented by the vector.
        glBufferData(GL_ARRAY_BUFFER, 3 * local_vertices.size() * sizeof(float), local_vertices.data(), GL_STATIC_DRAW);
    }


    // Now set up the bound hVAO
    // hVAO attribute 0 will describe the contents of (and link to) the object buffer.
    // The hVAO list position (first arg) will be used in shader code 'layout(location=0)'.
    glVertexAttribPointer(0, // location
                          3, // 3 floats per vertex
                          GL_FLOAT, GL_FALSE,
                          0, //3 * sizeof(float), //stride between each vertex.
                          (void*)0);
    // Enable this hVAO (list of lists) so its used for rendering during calls glDrawArrays, glDrawElements...etc.
    glEnableVertexAttribArray(0);
    // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // You can unbind the hVAO afterwards so other hVAO calls won't accidentally modify this hVAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    glBindVertexArray(0);
}

void Shape::draw() {
    // Draw into back buffer
    glBindVertexArray(hVAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
    glDrawArrays(getPrimitiveType(), 0, getVertexCount()); // start at vertex 0 -> vertexCount() for  triangles.
}
