//
// Created by navee on 04/07/2021.
//

#ifndef VULKAN_TEST_PARTICLESYSTEM_H
#define VULKAN_TEST_PARTICLESYSTEM_H

#include "ShaderManager.h"
#include <random>
#include <glm/gtc/matrix_transform.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>

namespace particles {
    typedef boost::array< double , 3 > state_type;

    template<class D>
    class ParticleSystem {
    public:
        void initializeSystem(int particleCount) {
            std::random_device device;
            std::mt19937 generator(device());
            std::uniform_real_distribution<float> distribution(-1.f, 1.f);

            particles.resize(particleCount);
            // Bug: must use ref for index else destructor will fire after each iteration, in turn releasing OGL buffers.
            for (D& particle : particles) {
                particle.setScale(glm::vec3(0.01f, 0.01f, 0.01f));
                particle.setWorldLocation(glm::vec3(distribution(generator), distribution(generator), distribution(generator)));
            }
        };

        void updateWorldLocations() {
            for (D& particle : particles) {
                glm::vec3 currentLoc = particle.getWorldLocation();
                boost::numeric::odeint::runge_kutta4<state_type> stepper;
                state_type x = { currentLoc.x, currentLoc.y, currentLoc.z}; // initial conditions
                state_type out;
                double t(0.0), dt(0.0005371);
                stepper.do_step(lorenz, x, t, out, dt);
                particle.setWorldLocation(glm::vec3(out[0], out[1], out[2]));
            }
        };

        static void lorenz( const state_type &x , state_type &dxdt , double t )
        {
            const double sigma = 10.0;
            const double R = 30.0;
            const double b = 8.0 / 3.0;

            dxdt[0] = sigma * ( x[1] - x[0] );
            dxdt[1] = R * x[0] - x[1] - x[0] * x[2];
            dxdt[2] = -b * x[2] + x[0] * x[1];
        };

        void draw(ShaderManager shader, glm::mat4 worldT) {
            for (D& particle : particles) {
                glm::mat4 local = particle.getScaleTransformation();
                glm::mat4 model = glm::translate(glm::mat4(1.f), particle.getWorldLocation());
                shader.setUniform("worldPlacementAndLocalT", worldT * model * local);
                particle.draw();
            }
        };

    private:
        std::vector<D> particles;
        bool localParticleTransformations = false;

    };
}
void lorenz2(glm::vec3& currentPos){
    //boost::numeric::odeint::runge_kutta_dopri5<state_type> stepper;

}
#endif //VULKAN_TEST_PARTICLESYSTEM_H
