//
// Created by navee on 07/07/2021.
//

#include "VerticesGenerator.h"
#include <vector>
#include <random>

std::vector<GLfloat> VerticesGenerator::triangles(int countTriangles) {
    std::random_device device;
    std::mt19937 generator(device());
    std::uniform_real_distribution<float> distribution(-1.f, 1.f);

    int countVertices = 3 * 2 * countTriangles;
    std::vector<GLfloat> vertices(countVertices);
    GLfloat triangleWidth = 0.01f;
    GLfloat triangleHeight = 0.01f;

    for (int i = 0; i < countVertices; i += 6) {
        GLfloat first_vertex_x = distribution(generator);
        GLfloat first_vertex_y = distribution(generator);
        GLfloat second_vertex_x = first_vertex_x + triangleWidth;
        GLfloat second_vertex_y = first_vertex_y;
        GLfloat third_vertex_x = first_vertex_x + triangleWidth/2.f;
        GLfloat third_vertex_y = first_vertex_y + triangleHeight;
        vertices[i] = first_vertex_x;
        vertices[i + 1] = first_vertex_y;
        vertices[i + 2] = second_vertex_x;
        vertices[i + 3] = second_vertex_y;
        vertices[i + 4] = third_vertex_x;
        vertices[i + 5] = third_vertex_y;
    }

    return vertices;
}
