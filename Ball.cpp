//
// Created by navee on 02/07/2021.
//

#include <iostream>
#include "Ball.h"

namespace particles {
    void Ball::initBuffers() {
        // Allocate one handle for a new hVAO (list of lists) and capture the handle.
        glCreateVertexArrays(1, &hVAO);
        // bind the list of lists so we can set its attributes during this call.
        glBindVertexArray(hVAO);

        // Allocate handle. todo: who will delete underlying objects?
        glGenBuffers(1, &hVBO);
        // Set the global state table's GL_ARRAY_BUFFER entry so following calls apply to this handle.
        glBindBuffer(GL_ARRAY_BUFFER, hVBO);

        // Allocate memory and copy in 'vertices' data.
        const std::vector<glm::vec3> &local_vertices = getLocalCoordsOfVertices();
        // We must calculate size of vector's contiguous memory in bytes.
        // vector.data(): elements in the vector are guaranteed to be stored in contiguous storage locations
        // in the same order as represented by the vector.
        glBufferData(GL_ARRAY_BUFFER, 3 * local_vertices.size() * sizeof(float), local_vertices.data(),
                     GL_STREAM_DRAW);


        // Now set up the bound hVAO
        // hVAO attribute 0 will describe the contents of (and link to) the object buffer.
        // The hVAO list position (first arg) will be used in shader code 'layout(location=0)'.
        glVertexAttribPointer(0, // location
                              3, // 3 floats per vertex
                              GL_FLOAT, GL_FALSE,
                              0, //3 * sizeof(float), //stride between each vertex.
                              (void *) 0);
        // Enable this hVAO (list of lists) so its used for rendering during calls glDrawArrays, glDrawElements...etc.
        glEnableVertexAttribArray(0);
        // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // You can unbind the hVAO afterwards so other hVAO calls won't accidentally modify this hVAO, but this rarely happens. Modifying other
        // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
        glBindVertexArray(0);
    }

    void Ball::draw() {
        // Draw into back buffer
        glBindVertexArray(
                hVAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
        glDrawArrays(getPrimitiveType(), 0, getVertexCount()); // start at vertex 0 -> vertexCount() for  triangles.
    }

    std::vector<glm::vec3> Ball::getLocalCoordsOfVertices() {
        if (originalLocalPlacement.empty()) {
            static const float pi = 3.141592654f;
            int noOfVertices = getVertexCount();
            float radius = 0.3f;

            for (int i = 0; i < noOfVertices; i++) {
                float angle = i * 2.f * pi / getVertexCount() - pi / 2.f;
                float x = std::cos(angle) * radius;
                float y = std::sin(angle) * radius;
                originalLocalPlacement.emplace_back(radius + x, radius + y, 0.f);
            }
        }

        return originalLocalPlacement;
    }

    int Ball::getVertexCount() {
        return 6;
    }

    void Ball::setWorldLocation(glm::vec3 newPos) {
        worldLocation = newPos;
    }

    void Ball::patchWorldLocation(glm::vec3 patch) {
        worldLocation = worldLocation + patch;
    }

    glm::vec3 Ball::getWorldLocation() {
        return worldLocation;
    }

    glm::mat4 Ball::getScaleTransformation() {
        glm::mat4 id(1.f);
        glm::mat4 S = glm::scale(id, scale);
        return S;
    }

    void Ball::setScale(glm::vec3 newScale) {
        scale = newScale;
    }
}