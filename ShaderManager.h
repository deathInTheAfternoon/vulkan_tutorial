//
// Created by navee on 28/06/2021.
//

#ifndef VULKAN_TEST_SHADERMANAGER_H
#define VULKAN_TEST_SHADERMANAGER_H

#include <glad/glad.h>
#include <filesystem>
#include <glm/glm.hpp>

class ShaderManager {
public:
    ShaderManager() {
        // program sent to GPU
        hShaderProgram = glCreateProgram();
    }

    void use(){
        glUseProgram(hShaderProgram);
    }

    void setUniform(std::string name, glm::mat4 value);

    ~ShaderManager(){
        glDeleteProgram(hShaderProgram);
    }
    void buildShaderProgram(const std::filesystem::path &vertexShader, const std::filesystem::path &fragShader);

private:
    GLuint hShaderProgram;

    std::string loadShaderFile(const std::filesystem::path &p);
    GLuint compileShader(const std::filesystem::path &p, GLenum type);
    void linkShadersToProgram(GLuint shaderProgram, GLuint vertexShader, GLuint fragmentShader);
};

#endif //VULKAN_TEST_SHADERMANAGER_H
