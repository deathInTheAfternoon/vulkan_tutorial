//
// Created by navee on 02/07/2021.
//

#include <iostream>
#include "DebugTriangle.h"

void DebugTriangle::initBuffers() {
    // Allocate one handle for a new hVAO (list of lists) and capture the handle.
    glCreateVertexArrays(1, &hVAO);
    // bind the list of lists so we can set its attributes during this call.
    glBindVertexArray(hVAO);

    // Allocate handle. todo: who will delete underlying objects?
    glGenBuffers(1, &hVBO);
    // Set the global state table's GL_ARRAY_BUFFER entry so following calls apply to this handle.
    glBindBuffer(GL_ARRAY_BUFFER, hVBO);

    // Allocate memory and copy in 'vertices' data. TODO: some examples currently use float*...hence this temporary decision
    const GLfloat* f_array = getFloatArray();
    if (f_array) {
        glBufferData(GL_ARRAY_BUFFER, 3 * getVertexCount() * sizeof(float), f_array, GL_STREAM_DRAW);
    } else {
        const std::vector<glm::vec3>& local_vertices = getLocalCoordsOfVertices();
        // We must calculate size of vector's contiguous memory in bytes.
        // vector.data(): elements in the vector are guaranteed to be stored in contiguous storage locations
        // in the same order as represented by the vector.
        glBufferData(GL_ARRAY_BUFFER, 3 * local_vertices.size() * sizeof(float), local_vertices.data(), GL_STREAM_DRAW);
    }

    // Now set up the bound hVAO
    // hVAO attribute 0 will describe the contents of (and link to) the object buffer.
    // The hVAO list position (first arg) will be used in shader code 'layout(location=0)'.
    glVertexAttribPointer(0, // location
                          3, // 3 floats per vertex
                          GL_FLOAT, GL_FALSE,
                          0, //3 * sizeof(float), //stride between each vertex.
                          (void*)0);
    // Enable this hVAO (list of lists) so its used for rendering during calls glDrawArrays, glDrawElements...etc.
    glEnableVertexAttribArray(0);
    // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // You can unbind the hVAO afterwards so other hVAO calls won't accidentally modify this hVAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    glBindVertexArray(0);
}

void DebugTriangle::draw() {
    // Draw into back buffer
    glBindVertexArray(hVAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
    glDrawArrays(getPrimitiveType(), 0, getVertexCount()); // start at vertex 0 -> vertexCount() for  triangles.
}

std::vector<glm::vec3> DebugTriangle::getLocalCoordsOfVertices() {
    std::vector<glm::vec3> ret(3);
    for (int i = 0; i < vertices.size(); i++) {
        ret[i] = glm::vec3(vertices[i]);
    }
    return ret;
}

int DebugTriangle::getVertexCount() {
    return 3;
}

void DebugTriangle::setWorldLocation(glm::vec3 newPos) {
    worldLocation = newPos;
}

void DebugTriangle::deltaWorldLocation(glm::vec3 deltas) {
    worldLocation = worldLocation + deltas;
}

glm::vec3 DebugTriangle::getLocation() {
    return worldLocation;
}

glm::mat4 DebugTriangle::getLocalTransformations() {
    glm::mat4 id(1.f);
    glm::mat4 S = glm::scale(id, scale);
    // We only rotate around local-origin. So if point of rotation is elswhere we must slide all of local space
    // so PoR sits over local-origin. Then rotate all of local space. Then shift all of local space so PoR sits over its
    // original location.
    glm::mat4 T = glm::translate(id, - pointOfTransformation);
    glm::mat4 R = glm::rotate(id, orientation, glm::vec3(0.f, 0.f, 1.f));
    glm::mat4 Ti = glm::translate(id, pointOfTransformation);

    // If we did S * Ti * ..., then point of rotation would scale correctly. However, it no longer matches the value
    // set by the user. ...R * T * S will leave the PoT where it is, and so the shape will rotate around a different origin.
    return Ti * R * T * S;
}

void DebugTriangle::incrementOrientation(float radians) {
    orientation = orientation + radians;
}

void DebugTriangle::setPointOfTransformation(glm::vec3 point) {
    pointOfTransformation = point;
}

void DebugTriangle::setScale(glm::vec3 newScale) {
    scale = newScale;
}

glm::vec3 DebugTriangle::getWorldLocation() {
    return worldLocation;
}
