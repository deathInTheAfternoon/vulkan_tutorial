
cmake_minimum_required(VERSION 3.2)
set(CMAKE_CXX_STANDARD 20)

project(vulkan-hpp-cube)

set(HEADERS
        )

set(SOURCES
        engine.cpp
        )

source_group(headers FILES ${HEADERS})
source_group(sources FILES ${SOURCES})

add_executable(vulkan-hpp-cube
        ${HEADERS}
        ${SOURCES}
        )

target_link_libraries(vulkan-hpp-cube ${GLFW_LIB} ${VULKAN_LIB} utils)
