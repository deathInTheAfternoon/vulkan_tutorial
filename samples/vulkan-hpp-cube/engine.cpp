#include <iostream>
#include <cstdint>
#include "vulkan/vulkan_raii.hpp"
#include <GLFW/glfw3.h> // MUST include after vulkan header.
#include <thread>
#include <fstream>
#include "../utils/math.hpp"
#include "../raii_utils/utils.hpp"
#include "../raii_utils/shaders.hpp"
#include "Public/ShaderLang.h"

static char const *AppName = "Vk HPP-RII Cube";
static char const *EngineName = "Vulkan.hpp";
const int window_height = 1080, window_width = 1920;
auto startTime = std::chrono::system_clock::now();

struct VertexPC {
    float x, y, z, w;   // Position
    float r, g, b, a;   // Color
};

static const VertexPC coloredCubeData[] = {
        // red face
        {-1.0f, -1.0f, 1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f},
        {-1.0f, 1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f},
        {1.0f,  -1.0f, 1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f},
        {1.0f,  -1.0f, 1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f},
        {-1.0f, 1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f},
        {1.0f,  1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f},
        // green face
        {-1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f},
        {1.0f,  -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f},
        {-1.0f, 1.0f,  -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f},
        {-1.0f, 1.0f,  -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f},
        {1.0f,  -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f},
        {1.0f,  1.0f,  -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f},
        // blue face
        {-1.0f, 1.0f,  1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f},
        {-1.0f, -1.0f, 1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f},
        {-1.0f, 1.0f,  -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f},
        {-1.0f, 1.0f,  -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f},
        {-1.0f, -1.0f, 1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f},
        {-1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f},
        // yellow face
        {1.0f,  1.0f,  1.0f,  1.0f, 1.0f, 1.0f, 0.0f, 1.0f},
        {1.0f,  1.0f,  -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f},
        {1.0f,  -1.0f, 1.0f,  1.0f, 1.0f, 1.0f, 0.0f, 1.0f},
        {1.0f,  -1.0f, 1.0f,  1.0f, 1.0f, 1.0f, 0.0f, 1.0f},
        {1.0f,  1.0f,  -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f},
        {1.0f,  -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f},
        // magenta face
        {1.0f,  1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 1.0f, 1.0f},
        {-1.0f, 1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 1.0f, 1.0f},
        {1.0f,  1.0f,  -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f},
        {1.0f,  1.0f,  -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f},
        {-1.0f, 1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 1.0f, 1.0f},
        {-1.0f, 1.0f,  -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f},
        // cyan face
        {1.0f,  -1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 1.0f, 1.0f},
        {1.0f,  -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f},
        {-1.0f, -1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 1.0f, 1.0f},
        {-1.0f, -1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 1.0f, 1.0f},
        {1.0f,  -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f},
        {-1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f},
};

static std::string readGLSLFile(const std::string &filename) {
    // ios::ate: open and seek to the end === file.seekg(0,std::ios::end). Now tellg() returns file size.
    std::ifstream file(filename, std::ios::ate | std::ios::binary);
    if (!file.is_open()) {
        throw std::runtime_error("failed to open file!");
    }
    size_t fileSize = (size_t) file.tellg();
    std::string buffer(fileSize, ' '); // fill with blanks
    file.seekg(0); // Move to start
    file.read(buffer.data(), fileSize);
    file.close();
    return buffer;
}

// utils.hpp can only allocate a single buffer...we need bufferS.
vk::raii::CommandBuffers makeCommandBuffers(vk::raii::Device const &device,
                                            vk::raii::CommandPool const &commandPool, uint32_t commandBufferCount = 1) {
    vk::CommandBufferAllocateInfo commandBufferAllocateInfo(*commandPool, vk::CommandBufferLevel::ePrimary,
                                                            commandBufferCount);
    // Raii already includes method to generate multiple command buffer objects.
    return vk::raii::CommandBuffers(device, commandBufferAllocateInfo);
}

// Called once to setup each command buffer that is associated with a framebuffer.
static void initCommandBuffers(vk::raii::su::SurfaceData &surfaceData, const vk::raii::CommandBuffer &commandBuffer,
                               const vk::raii::PipelineLayout &pipelineLayout, const vk::raii::RenderPass &renderPass,
                               const std::vector<vk::raii::Framebuffer> &framebuffers,
                               const vk::raii::su::BufferData &vertexBufferData,
                               const vk::raii::DescriptorSet &descriptorSet, const vk::raii::Pipeline &graphicsPipeline,
                               const vk::raii::CommandBuffers &commandBuffers,
                               const vk::raii::Device &device, const vk::raii::CommandPool &commandPool) {

    // Loop through framebuffers
    for (int i = 0; i < framebuffers.size(); i++) {
        const vk::raii::CommandBuffer &commandBuffer = commandBuffers[i];

        std::array<vk::ClearValue, 2> clearValues;
        clearValues[0].color = vk::ClearColorValue(std::array<float, 4>({{0.2f, 0.2f, 0.2f, 0.2f}}));
        clearValues[1].depthStencil = vk::ClearDepthStencilValue(1.0f, 0);

        vk::RenderPassBeginInfo renderPassBeginInfo(
                *renderPass, *framebuffers[i], vk::Rect2D(vk::Offset2D(0, 0), surfaceData.extent),
                clearValues);

        commandBuffer.begin({});
        commandBuffer.beginRenderPass(renderPassBeginInfo, vk::SubpassContents::eInline);
        commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, *graphicsPipeline);
        commandBuffer.bindDescriptorSets(
                vk::PipelineBindPoint::eGraphics, *pipelineLayout, 0, {*descriptorSet}, nullptr);

        commandBuffer.bindVertexBuffers(0, {*vertexBufferData.buffer}, {0});
        commandBuffer.setViewport(0,
                                  vk::Viewport(0.0f,
                                               0.0f,
                                               static_cast<float>( surfaceData.extent.width ),
                                               static_cast<float>( surfaceData.extent.height ),
                                               0.0f,
                                               1.0f));
        commandBuffer.setScissor(0, vk::Rect2D(vk::Offset2D(0, 0), surfaceData.extent));

        commandBuffer.draw(12 * 3, 1, 0, 0);
        commandBuffer.endRenderPass();
        commandBuffer.end();
    }
}

static void
updateUniformBuffer(const vk::raii::PhysicalDevice &physicalDevice, const vk::raii::su::SurfaceData &surfaceData,
                    const vk::raii::Device &device, vk::raii::CommandPool &commandPool, vk::raii::Queue &queue,
                    vk::raii::su::BufferData &uniformBufferOnDevice, float wPosIncrement = 0.0f) {

    glm::mat4x4 pvmMatrix = vk::su::createModelViewProjectionClipMatrix(surfaceData.extent, wPosIncrement);
    uniformBufferOnDevice.upload(physicalDevice, device, commandPool, queue, std::vector<glm::mat4x4>{pvmMatrix},
                                 0);//stride = 0 as buffer is tightly packed.
}

static void updateUTime(const vk::raii::PhysicalDevice &physicalDevice, const vk::raii::su::SurfaceData &surfaceData,
                        const vk::raii::Device &device, vk::raii::CommandPool &commandPool, vk::raii::Queue &queue,
                        vk::raii::su::BufferData &uniformBufferOnDevice) {

    auto now = std::chrono::system_clock::now();
    std::chrono::duration<float> sDuration = now - startTime;
    uniformBufferOnDevice.upload(physicalDevice, device, commandPool, queue, std::vector<float>{sDuration.count()},
                                 0);//stride = 0 as buffer is tightly packed.
}

#if !defined( NDEBUG )

// Debug Messenger's entry point into our code. Only used if code below is uncommented.
static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
        void *pUserData) {

    std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

    return VK_FALSE;
}

#endif

static int initVulkanRII(GLFWwindow *window, const std::vector<std::string> &exts, uint32_t numberExts) {
    // This object loads "vulkan-1.dll" and sets up the first five function pointers:  vkInstanceGetProcAddr,
    // vkCreateInstance, vkEnumerateInstanceExtensionProperties, vkEnumerateInstanceLayerProperties, vkEnumerateInstanceVersion.
    // We do not call these directly. They are used by 'smart handles'.
    try {
        vk::raii::Context context;
        vk::raii::Instance instance =
                vk::raii::su::makeInstanceNT(context, AppName, EngineName, {}, exts);

        // This code connects VK debug messenger to our callback above. We need eInfo flag to pick up shader debugPrintfEXT()
        // messages. To avoid excess messages we have disabled all other ValidationEXTs in makeInstanceCreateInfoChainNT().
#if !defined( NDEBUG )
        vk::DebugUtilsMessengerCreateInfoEXT callbackDetails = vk::su::makeDebugUtilsMessengerCreateInfoEXT();
        callbackDetails.setMessageSeverity(vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo);
        callbackDetails.setPfnUserCallback(debugCallback);
        vk::raii::DebugUtilsMessengerEXT debugUtilsMessenger(instance, callbackDetails);
#endif
        vk::raii::PhysicalDevice physicalDevice = std::move(vk::raii::PhysicalDevices(instance).front());

        // starts up glfw context, window and surface.
        vk::raii::su::SurfaceData surfaceData(instance, AppName, vk::Extent2D(window_width, window_height));
        // Get the surface object and find its capabilities...we need the minImageCount later. Note: this is a min, the implementation may
        // force more on you - e.g. if triple buffering were on and you have set minImageCount=2, you can find 3 images in the swapchain.
        vk::raii::SurfaceKHR &rSurface = *surfaceData.pSurface;
        vk::SurfaceCapabilitiesKHR capabilitiesKhr = physicalDevice.getSurfaceCapabilitiesKHR(*rSurface);
        uint32_t imageCount =
                capabilitiesKhr.minImageCount + 1; // Always make room for an extra image...according to docs

        std::pair<uint32_t, uint32_t> graphicsAndPresentQueueFamilyIndex =
                vk::raii::su::findGraphicsAndPresentQueueFamilyIndex(physicalDevice, *surfaceData.pSurface);
        vk::raii::Device device = vk::raii::su::makeDevice(
                physicalDevice, graphicsAndPresentQueueFamilyIndex.first, vk::su::getDeviceExtensions());

        vk::raii::CommandPool commandPool = vk::raii::CommandPool(
                device, {vk::CommandPoolCreateFlagBits::eResetCommandBuffer, graphicsAndPresentQueueFamilyIndex.first});
        vk::raii::CommandBuffer commandBuffer = vk::raii::su::makeCommandBuffer(device, commandPool);
        //NT: Switch to commandbuffers...one for each swapchain image.
        vk::raii::CommandBuffers commandBuffers = makeCommandBuffers(device, commandPool, imageCount);

        vk::raii::Queue graphicsQueue(device, graphicsAndPresentQueueFamilyIndex.first, 0);
        vk::raii::Queue presentQueue(device, graphicsAndPresentQueueFamilyIndex.second, 0);

        // This will set the swapchain size to surfaceCapabilities.minImageCount.
        vk::raii::su::SwapChainData swapChainData(physicalDevice,
                                                  device,
                                                  *surfaceData.pSurface,
                                                  surfaceData.extent,
                                                  vk::ImageUsageFlagBits::eColorAttachment |
                                                  vk::ImageUsageFlagBits::eTransferSrc,
                                                  {},
                                                  graphicsAndPresentQueueFamilyIndex.first,
                                                  graphicsAndPresentQueueFamilyIndex.second);

        vk::raii::su::DepthBufferData depthBufferData(physicalDevice, device, vk::Format::eD16Unorm,
                                                      surfaceData.extent);

        // ISSUE: BufferData uses a memory allocation per instance - hence no mem sharing between buffers which is inefficient.
        // BufferData() calls Buffer::vkCreateBuffer, check device supports usage and mem flags
        // (note: current util.hpp does not handle failure case), DeviceMemory::vkAllocateMemory, DeviceMemory:: vkBindBufferMemory
        // This uniform buffer lives on the device.
        vk::raii::su::BufferData uniformBufferData(physicalDevice, device, sizeof(glm::mat4x4),
                                                   vk::BufferUsageFlagBits::eTransferDst |
                                                   vk::BufferUsageFlagBits::eUniformBuffer,
                                                   vk::MemoryPropertyFlagBits::eDeviceLocal);

        // Init first PVM matrix and copy to device memory.
        updateUniformBuffer(physicalDevice, surfaceData, device, commandPool, graphicsQueue, uniformBufferData);

        // Repeat for sending time to fragment shader
        vk::raii::su::BufferData uTime(physicalDevice, device, 68 /*sizeof(float)*/,
                                       vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eUniformBuffer,
                                       vk::MemoryPropertyFlagBits::eDeviceLocal);
        updateUTime(physicalDevice, surfaceData, device, commandPool, graphicsQueue, uTime);

        // A descriptor is a pointer into a resource e.g. buffer or image. These resources will be unmarshalled into shader global variables or blocks.
        // A descriptor set is used to group descriptors.
        // A DSL is a schema for a descriptor set. An array of DSLBindings define each descriptor that must be bound
        // to a resource (e.g buffer or image) in the instantiated Descriptor Set.
        // WARNING: No matter the destination stage of a particular descriptor all prior descriptors in that set must be bound (even with dummy data).
        vk::raii::DescriptorSetLayout descriptorSetLayout = vk::raii::su::makeDescriptorSetLayout(
                device, {{vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex},
                         {vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eFragment}});
        vk::raii::PipelineLayout pipelineLayout(device, {{}, *descriptorSetLayout});

        vk::Format colorFormat =
                vk::su::pickSurfaceFormat(physicalDevice.getSurfaceFormatsKHR(**surfaceData.pSurface)).format;
        vk::raii::RenderPass renderPass = vk::raii::su::makeRenderPass(device, colorFormat, depthBufferData.format);

        /* Set up vertex and fragment shader */
        std::string vertexShaderCode = readGLSLFile(
                "C:\\Users\\navee\\CLionProjects\\vulkan_test\\samples\\vulkan-hpp-cube\\shaders\\vertex.vert");
        std::string fragmentShaderCode = readGLSLFile(
                "C:\\Users\\navee\\CLionProjects\\vulkan_test\\samples\\vulkan-hpp-cube\\shaders\\roundCells.frag");

        glslang::InitializeProcess(); // Init once per process

        std::vector<unsigned int> vertexShaderSPV;
        bool ok = vk::su::GLSLtoSPV(vk::ShaderStageFlagBits::eVertex, vertexShaderCode, vertexShaderSPV);
        assert(ok);

        // WARNING: We need the size in bytes - so multiply by 4, not sure why?
        vk::ShaderModuleCreateInfo smciVert = vk::ShaderModuleCreateInfo(
                vk::ShaderModuleCreateFlags(),
                4 * vertexShaderSPV.size(),
                reinterpret_cast<const uint32_t *>(vertexShaderSPV.data()));
        vk::raii::ShaderModule vertexShaderModule = vk::raii::ShaderModule(device, smciVert);

        std::vector<unsigned int> fragmentShaderSPV;
        ok = vk::su::GLSLtoSPV(vk::ShaderStageFlagBits::eFragment, fragmentShaderCode, fragmentShaderSPV);
        assert(ok);

        glslang::FinalizeProcess();
        /* End shader setup */

        vk::ShaderModuleCreateInfo smciFrag = vk::ShaderModuleCreateInfo(
                vk::ShaderModuleCreateFlags(),
                4 * fragmentShaderSPV.size(),
                reinterpret_cast<const uint32_t *>(fragmentShaderSPV.data()));
        vk::raii::ShaderModule fragmentShaderModule = vk::raii::ShaderModule(device, smciFrag);

        // Reserves imageViews.size() framebuffers.
        std::vector<vk::raii::Framebuffer> framebuffers = vk::raii::su::makeFramebuffers(
                device, renderPass, swapChainData.imageViews, &*depthBufferData.pImageView, surfaceData.extent);

        vk::raii::su::BufferData vertexBufferData(
                physicalDevice, device, sizeof(coloredCubeData), vk::BufferUsageFlagBits::eVertexBuffer);
        vk::raii::su::copyToDevice(
                vertexBufferData.deviceMemory, coloredCubeData, sizeof(coloredCubeData) / sizeof(coloredCubeData[0]));

        vk::raii::DescriptorPool descriptorPool =
                vk::raii::su::makeDescriptorPool(device, {{vk::DescriptorType::eUniformBuffer, 2}});
        // NT: A descriptor set is set of resourced bound as a group.
        // The structure of each set is defined by a Descriptor layout which describes the order and types of resources in the set.
        // The set of all layouts is called the pipeline layout. Descriptor Sets and Pipelines are created with reference to their layouts.
        // Note: 'the order' of a layout implies each descriptor defined by a layout is mandatory.
        // ISSUE: uses .front() because its only creating a single descriptor set.
        vk::raii::DescriptorSet descriptorSet =
                std::move(vk::raii::DescriptorSets(device, {*descriptorPool, *descriptorSetLayout}).front());
        vk::raii::su::updateDescriptorSets(
                device, descriptorSet, {{vk::DescriptorType::eUniformBuffer, uniformBufferData.buffer, nullptr},
                                        {vk::DescriptorType::eUniformBuffer, uTime.buffer,             nullptr}}, {});

        vk::raii::PipelineCache pipelineCache(device, vk::PipelineCacheCreateInfo());
        // Changes vk default assembly from ePointsList to eTriangleList.
        vk::raii::Pipeline graphicsPipeline = vk::raii::su::makeGraphicsPipeline(
                device,
                pipelineCache,
                vertexShaderModule, // Assigns shader to specific shader stage of pipeline (eVertex)
                nullptr,
                fragmentShaderModule, // Assigns shader to specific shader stage of pipeline (eFragment)
                nullptr,
                vk::su::checked_cast<uint32_t>(sizeof(coloredCubeData[0])),
                {{vk::Format::eR32G32B32A32Sfloat, 0},
                 {vk::Format::eR32G32B32A32Sfloat, 16}},
                vk::FrontFace::eClockwise,
                true,
                pipelineLayout,
                renderPass);

        initCommandBuffers(surfaceData, commandBuffer, pipelineLayout, renderPass, framebuffers,
                           vertexBufferData, descriptorSet,
                           graphicsPipeline,
                           commandBuffers, device, commandPool);

        float testIncr = 0.f;
        GLFWwindow *hWindow = surfaceData.window.handle;
        while (!glfwWindowShouldClose(hWindow)) {
            glfwPollEvents();
            // Copy new PVM matrix to device memory
            updateUniformBuffer(physicalDevice, surfaceData, device, commandPool, graphicsQueue, uniformBufferData,
                                testIncr += 0.01f);
            updateUTime(physicalDevice, surfaceData, device, commandPool, graphicsQueue, uTime);
            // Get the index of the next available swapchain image:
            vk::raii::Semaphore imageAcquiredSemaphore(device, vk::SemaphoreCreateInfo());
            vk::Result result;
            uint32_t imageIndex;
            std::tie(result, imageIndex) =
                    swapChainData.pSwapChain->acquireNextImage(vk::su::FenceTimeout, *imageAcquiredSemaphore);
            assert(result == vk::Result::eSuccess);
            assert(imageIndex < swapChainData.images.size());

            // Send graphics command to the device.
            // Fence is used to signal completion of this set of commands on the GPU.
            vk::raii::Fence drawFence(device, vk::FenceCreateInfo());
            // The final colors are output from the pipeline at the eColorAttachmentOutput stage. Our fence will wait for this event so
            // we know image is ready to be sent to the display.
            vk::PipelineStageFlags waitDestinationStageMask(vk::PipelineStageFlagBits::eColorAttachmentOutput);
            vk::SubmitInfo submitInfo(*imageAcquiredSemaphore, waitDestinationStageMask, *commandBuffers[imageIndex]);
            graphicsQueue.submit(submitInfo, *drawFence);

            // Without this you violate the spec: 'All queue submission commands referring to 'fence' must have completed'
            while (vk::Result::eTimeout == device.waitForFences({*drawFence}, VK_TRUE, vk::su::FenceTimeout));

            // Now image in swap chain is ready, we can present it to the user.
            vk::PresentInfoKHR presentInfoKHR(nullptr, **swapChainData.pSwapChain, imageIndex);
            result = presentQueue.presentKHR(presentInfoKHR);
            switch (result) {
                case vk::Result::eSuccess:
                    break;
                case vk::Result::eSuboptimalKHR:
                    std::cout << "vk::Queue::presentKHR returned vk::Result::eSuboptimalKHR !\n";
                    break;
                default:
                    assert(false);  // an unexpected result is returned !
            }
        }

        device.waitIdle();
    }
    catch (vk::SystemError &err) {
        std::cout << "vk::SystemError: " << err.what() << std::endl;
        exit(-1);
    }
    catch (std::exception &err) {
        std::cout << "std::exception: " << err.what() << std::endl;
        exit(-1);
    }
    catch (...) {
        std::cout << "unknown error\n";
        exit(-1);
    }
    return 0;
}

int main() {
    // We're going to get glfw to use Vulkan
    glfwInit();
    // A window holds a context. In fact the window handle = context handle.
    // A context is created for a particular client API e.g. OpenGL.
    // This context is for 'no api' as we're using Vulkan.
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    // todo: Above code creates its own glfw window...what if we don't want that?
    //GLFWwindow* window = glfwCreateWindow(1920, 1080, "Vulkan example", NULL, NULL);
    // Check for vulkan-1.dll (loader) and an ICD
    if (!glfwVulkanSupported()) {
        std::cout << "Vulkan loader (vulkan-1.dll) or ICD not found" << std::endl;
        return -1;
    }
    // GLFW can list names of Vulkan extensions it needs to draw on Vulkan surfaces.
    uint32_t how_many_extensions_for_glfw = 0;
    const char **glfw_requires_these_extensions = glfwGetRequiredInstanceExtensions(&how_many_extensions_for_glfw);
    std::vector<std::string> exts;
    for (int i = 0; i < how_many_extensions_for_glfw; i++)
        exts.emplace_back(glfw_requires_these_extensions[i]);

    initVulkanRII(nullptr, exts, how_many_extensions_for_glfw);

    return 1;
}


