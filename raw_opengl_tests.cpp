//
// Created by navee on 06/07/2021.
//

#include "raw_opengl_tests.h"
#include <glad/glad.h>
#include "ShaderManager.h"
#include "VerticesGenerator.h"
#include <GLFW/glfw3.h>
#include <filesystem>
#include <fstream>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace raw_opengl_tests {
    int screen_width = 1920, screen_height = 1080;
    const int countTriangles = 1000;
    GLuint hRawBuffer;
    GLuint hVAO;
    std::vector<GLfloat> vertices;

    void static framebuffer_size_callback(GLFWwindow* window, int width, int height)
    {
        // make sure the viewport matches the new window dimensions; note that width and
        // height will be significantly larger than specified on retina displays.
        glViewport(0, 0, width, height);
    }

    GLFWwindow* standardOpenGLInit() {
        glfwInit();

        GLFWwindow *window = glfwCreateWindow(screen_width, screen_height, "GLFW Tests", nullptr, nullptr);

        glfwMakeContextCurrent(window);
        glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

        gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);

        return window;
    }


    GLuint createVAOusingDSA() {
        vertices = VerticesGenerator::triangles(countTriangles);
        int sizeInBytes = vertices.size() * sizeof(decltype(vertices)::value_type);

        glCreateBuffers(1, &hRawBuffer);
        // Allocate space and copy in data...
        //sizeof(T*) isn't reliable. countTriangles * 3 vertices/tri * 2 floats/vert * 4 bytes/float gives accurate byte length.
        glNamedBufferStorage(hRawBuffer,
                             sizeInBytes, // Number of bytes to allocate for this buffer.
                             vertices.data(), // Data to be copied into the buffer.
                             GL_DYNAMIC_STORAGE_BIT); // '0' means immutable. This flag means mutable with glBufferSubData

        glCreateVertexArrays(1, &hVAO);

        // Connect vertex buffer to its parent vertex array object.
        glVertexArrayVertexBuffer(hVAO,
                                  0,
                                  hRawBuffer, // Raw data for attribute 0 is in hRawBuffer
                                  0,
                                  2 * sizeof(float)); // One vertex is made of two floats
        // And here's how attribute 0 should interpret the raw data - 2 floats per element.
        glVertexArrayAttribFormat(hVAO, 0, 2, GL_FLOAT, GL_FALSE, 0);
        glEnableVertexArrayAttrib(hVAO, 0); //shader will pull location 0 from this array instead of a constant value.

        return hVAO;
    }

    GLuint createVAO2() {
        GLuint hVAO;
        static const GLfloat vertices[12] = {
                -0.9f, -.9f,
                .85f, -.9f,
                -.9f, .85f,
                0.9f, -.85f, //Triangle 2
                .9f, .9,
                -.85f, .9
        };

        glCreateBuffers(1, &hRawBuffer);
        glNamedBufferStorage(hRawBuffer, 48, vertices, 0);
        glBindBuffer(GL_ARRAY_BUFFER, hRawBuffer);
        glGenVertexArrays(1, &hVAO);
        glBindVertexArray(hVAO); // without DSA you have to bind VAO to modify it. Of course, you must bind before use in drawArrays.
        //glBindBuffer(GL_ARRAY_BUFFER, hRawBuffer);//without DSA you have bind hRawBuffer to modify it.
        // Link slot '0' in vertex shader with buffer binding point and format buffer.
        glVertexAttribPointer(0, //point slot 0 at currently bound vertex array.
                              2, //each element of the bound vertex array has two values e.g. [(x,y),...]
                              GL_FLOAT, //each value of an element is a float e.g. (1.f, 2.f)
                              GL_FALSE, //do not constrain within [-1,1].
                              0, // each array element is packed adjacent to the next element, so stride is zero.
                              0); // array begins at start of buffer so offset = 0.
        glEnableVertexAttribArray(0); //Activate

        return hVAO;
    }

    // todo: update positions then overwrite the buffer.
    int main() {
        GLFWwindow* window = standardOpenGLInit();
        GLuint hVAO = createVAOusingDSA();

        std::filesystem::path v("..\\shaders\\raw_opengl_tests_vertex.glsl");
        std::filesystem::path f("..\\shaders\\fragment.glsl");
        ShaderManager shaderManager;
        shaderManager.buildShaderProgram(v, f);

        float rotationIncrement = 0.1f;
        float rotation = 0.f;
        while (!glfwWindowShouldClose(window)) {
            glClearColor(0.f, 0.f, 100.f, 0.f);
            glClear(GL_COLOR_BUFFER_BIT);

            // todo: update the position of all particles.
            for (int i = 0; i < vertices.size(); i += 2) {
                vertices[i] = vertices[i] + .001f;
            }
            int sizeInBytes = vertices.size() * sizeof(decltype(vertices)::value_type);
            glNamedBufferSubData(hRawBuffer, 0, sizeInBytes, vertices.data());

            rotation += rotationIncrement;
            glm::mat4 rotationMatrix = glm::rotate(glm::mat4(1.f), glm::radians(rotation), glm::vec3(0.f, 0.f, 1.f));

            shaderManager.use();
            shaderManager.setUniform("T", glm::mat4(1.f)/*rotationMatrix*/);
            glBindVertexArray(hVAO);
            glDrawArrays(GL_TRIANGLES, 0, 3 * countTriangles);
            glBindVertexArray(0);

            glfwSwapBuffers(window);
            // Check for events, update window and run callback.
            glfwPollEvents();
        }

        glfwTerminate();
        return 0;
    }
}