#include <iostream>
#include <cstdint>
#include "vulkan/vulkan_raii.hpp"
#include <GLFW/glfw3.h> // MUST include after vulkan header.
#include <thread>
#include <fstream>
#include "../utils/math.hpp"
#include "../raii_utils/utils.hpp"
#include "../raii_utils/shaders.hpp"
#include "Public/ShaderLang.h"
#include "particle_system/ParticleSystem.h"
#include "SimpleIni.h"
particles::ParticleSystem ps;

static char const *AppName = "Vk HPP-RII Particles";
static char const *EngineName = "Vulkan.hpp";
const int window_height = 1080, window_width = 1920;
auto startTime = std::chrono::system_clock::now();

// We need a flat contiguous array holding the positions of all the vertices. A single vector is a contiguous structure.
// vector.size() gives number of elements (element could a float or a vec4). sizeof(vector[0]) gives size in bytes of a single element
// (float = 4 bytes, vec4 = 16 bytes). Total number of bytes needed by C array = vector.size() * sizeof(vector[0]).
// Presently: sizeof(VertexNT) = 48 bytes (3 vertices * 4 floats/vert * 4 bytes/float).
using namespace particles;
//todo: following should be references to avoid copying, I believe?
static std::vector<Vertex> combinedVertices;
static std::vector<uint32_t> indexBuffer;
static int PARTICLE_COUNT;
static int TRAIL_LENGTH;
static int TRAIL_SAMPLE_RATE;

static std::string readGLSLFile(const std::string &filename) {
    // ios::ate: open and seek to the end === file.seekg(0,std::ios::end). Now tellg() returns file size.
    std::ifstream file(filename, std::ios::ate | std::ios::binary);
    if (!file.is_open()) {
        throw std::runtime_error("failed to open file!");
    }
    size_t fileSize = (size_t) file.tellg();
    std::string buffer(fileSize, ' '); // fill with blanks
    file.seekg(0); // Move to start
    file.read(buffer.data(), fileSize);
    file.close();
    return buffer;
}

static void loadIniFile(const std::string &filename) {
    CSimpleIni ini;
    SI_Error rc = ini.LoadFile(filename.c_str());
    if (rc < 0) {
        throw std::runtime_error("Failed to open ini file.");
    }
    const char *pValue = ini.GetValue("particles", "particleCount");
    PARTICLE_COUNT = std::stoi(pValue);
    pValue = ini.GetValue("particles", "trailLength");
    TRAIL_LENGTH = std::stoi(pValue);
    pValue = ini.GetValue("particles", "trailSampleRate");
    TRAIL_SAMPLE_RATE = std::stoi(pValue);
}

// utils.hpp can only allocate a single buffer...we need bufferS.
vk::raii::CommandBuffers makeCommandBuffers(vk::raii::Device const &device,
                                            vk::raii::CommandPool const &commandPool, uint32_t commandBufferCount = 1) {
    vk::CommandBufferAllocateInfo commandBufferAllocateInfo(*commandPool, vk::CommandBufferLevel::ePrimary,
                                                            commandBufferCount);
    // Raii already includes method to generate multiple command buffer objects.
    return vk::raii::CommandBuffers(device, commandBufferAllocateInfo);
}

// Called once to setup each command buffer that is associated with a framebuffer. Each CommandBuffer knows its Framebuffer
// and thus, indirectly, which Swapchain VkImage it has to construct before display. For this to work, the index for the
// each Framebuffer, CommandBuffer and Swapchain image must align.
static void initCommandBuffers(vk::raii::su::SurfaceData &surfaceData, const vk::raii::CommandBuffer &commandBuffer,
                               const vk::raii::PipelineLayout &pipelineLayout, const vk::raii::RenderPass &renderPass,
                               const std::vector<vk::raii::Framebuffer> &framebuffers,
                               const vk::raii::su::BufferData &vertexBufferData, const vk::raii::su::BufferData &indexBuffer,
                               const vk::raii::DescriptorSet &descriptorSet,
                               const vk::raii::Pipeline &gpTriangleList, const vk::raii::Pipeline &gpLineList,
                               const vk::raii::CommandBuffers &commandBuffers,
                               const vk::raii::Device &device, const vk::raii::CommandPool &commandPool) {

    // NT: We're essentially writing a script to process each Framebuffer which means processing each Swapchain VkImage.
    // Post: framebuffer.size() == swapchain.size(). Each Framebuffer -> [swapchain VkImageView, depth buffer VkImageView]
    for (int i = 0; i < framebuffers.size(); i++) {
        const vk::raii::CommandBuffer &commandBuffer = commandBuffers[i];

        std::array<vk::ClearValue, 2> clearValues;
        clearValues[0].color = vk::ClearColorValue(std::array<float, 4>({{0.2f, 0.2f, 0.2f, 0.2f}}));
//        The initial value at each point in the depth buffer should be the furthest possible depth, which is 1.0.
        clearValues[1].depthStencil = vk::ClearDepthStencilValue(1.0f, 0);

        // NT: This framebuffer contains a matching Swapchain VkImageView (i.e. 'i' also refers to SwapChain VkImage 'i') and the depth buffer VkImageView.
        // This set of commands will reference the VkImageLayouts in the corresponding Framebuffer.
        vk::RenderPassBeginInfo renderPassBeginInfo(
                *renderPass, *framebuffers[i], vk::Rect2D(vk::Offset2D(0, 0), surfaceData.extent),
                clearValues);

        // This is the graphics workflow we wish VK to execute.
        // Before recording command into the buffer, reset it to an initial state.
        commandBuffer.begin({});
        commandBuffer.beginRenderPass(renderPassBeginInfo, vk::SubpassContents::eInline);
        // NT: CommandBuffers have 4 slots where specific types of pipeline can be bound. Appropriate types of commands are then sent to those binding points.
        // DrawX commands are sent to the pipeline bound at GRAPHICS bind point. DispatchX commands are sent to the COMPUTE pipeline bind point.
        // Similarly for RAY_TRACING bind point. This command specifies were binding 'graphicsPipeline' as a GRAPHICS pipeline.
        commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, *gpTriangleList);
        commandBuffer.bindDescriptorSets(
                vk::PipelineBindPoint::eGraphics, *pipelineLayout, 0, {*descriptorSet}, nullptr);

        commandBuffer.bindVertexBuffers(0, {*vertexBufferData.buffer}, {0});
        commandBuffer.setViewport(0,
                                  vk::Viewport(0.0f,
                                               0.0f,
                                               static_cast<float>( surfaceData.extent.width ),
                                               static_cast<float>( surfaceData.extent.height ),
                                               0.0f,
                                               1.0f));
        commandBuffer.setScissor(0, vk::Rect2D(vk::Offset2D(0, 0), surfaceData.extent));

        // NT: Draw primitives. Assembles vertices according to 'current primitive'. Different primitive -> different graphics pipeline.
        commandBuffer.draw(ps.getNumberOfParticleVertices(),
                           1, // How many instances per draw call.
                           0,// First primitive starts at first vertex.
                           0); // Used when instancing.


       // Bind the second pipeline which uses a different primitive topology.
        commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, *gpLineList);
        // We're going to use indexed drawing. Required so VK knows where LineStrips end.
        // Rebind so next draw call knows where LineStrip data is held.
        commandBuffer.bindVertexBuffers(0, {*vertexBufferData.buffer}, {ps.getByteOffsetOfTrailData()});
        // The 'indexType' must cope with the total number of vertices composing all the trails e.g. eUint16 -> max 65535.
        // Naturally the index buffer returned from the particle system must match this type.
        commandBuffer.bindIndexBuffer(*indexBuffer.buffer, 0, vk::IndexType::eUint32);
       commandBuffer.drawIndexed(ps.getIndexBufferSize(), // total number of indices of 'indexType'.
                                 1,
                                 0,
                                 0, // We've already offset past triangles in 'bindVertexBuffers'
                                 0);

        commandBuffer.endRenderPass();
        commandBuffer.end();
    }
}

static void
updateUniformBuffer(const vk::raii::PhysicalDevice &physicalDevice, const vk::raii::su::SurfaceData &surfaceData,
                    const vk::raii::Device &device, vk::raii::CommandPool &commandPool, vk::raii::Queue &queue,
                    vk::raii::su::BufferData &uniformBufferOnDevice, float wPosIncrement = 0.0f) {

    glm::mat4x4 pvmMatrix = vk::su::createModelViewProjectionClipMatrix(surfaceData.extent, wPosIncrement);
    uniformBufferOnDevice.upload(physicalDevice, device, commandPool, queue, std::vector<glm::mat4x4>{pvmMatrix},
                                 0);//stride = 0 as buffer is tightly packed.
}

static void updateUTime(const vk::raii::PhysicalDevice &physicalDevice, const vk::raii::su::SurfaceData &surfaceData,
                        const vk::raii::Device &device, vk::raii::CommandPool &commandPool, vk::raii::Queue &queue,
                        vk::raii::su::BufferData &uniformBufferOnDevice) {

    auto now = std::chrono::system_clock::now();
    std::chrono::duration<float> sDuration = now - startTime;
    uniformBufferOnDevice.upload(physicalDevice, device, commandPool, queue, std::vector<float>{sDuration.count()},
                                 0);//stride = 0 as buffer is tightly packed.
}

#if !defined( NDEBUG )

// Debug Messenger's entry point into our code. Only used if code below is uncommented.
static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
        void *pUserData) {

    std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

    return VK_FALSE;
}

#endif

vk::PrimitiveTopology topTriangleList = vk::PrimitiveTopology::eTriangleList;
vk::PrimitiveTopology topTrails = vk::PrimitiveTopology::eLineStrip;

static int initVulkanRII(GLFWwindow *window, const std::vector<std::string> &exts, uint32_t numberExts) {
    ps.initializeSystem(PARTICLE_COUNT, TRAIL_LENGTH, TRAIL_SAMPLE_RATE);
    combinedVertices = ps.getAllVertices();
    indexBuffer = ps.getIndexBuffer();

    // This object loads "vulkan-1.dll" and sets up the first five function pointers:  vkInstanceGetProcAddr,
    // vkCreateInstance, vkEnumerateInstanceExtensionProperties, vkEnumerateInstanceLayerProperties, vkEnumerateInstanceVersion.
    // We do not call these directly. They are used by 'smart handles'.
    try {
        vk::raii::Context context;
        vk::raii::Instance instance =
                vk::raii::su::makeInstanceNT(context, AppName, EngineName, {}, exts);

        // This code connects VK debug messenger to our callback above. We need eInfo flag to pick up shader debugPrintfEXT()
        // messages. To avoid excess messages we have disabled all other ValidationEXTs in makeInstanceCreateInfoChainNT().
#if !defined( NDEBUG )
        vk::DebugUtilsMessengerCreateInfoEXT callbackDetails = vk::su::makeDebugUtilsMessengerCreateInfoEXT();
        callbackDetails.setMessageSeverity(vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo);
        callbackDetails.setPfnUserCallback(debugCallback);
        vk::raii::DebugUtilsMessengerEXT debugUtilsMessenger(instance, callbackDetails);
#endif
        vk::raii::PhysicalDevice physicalDevice = std::move(vk::raii::PhysicalDevices(instance).front());

        // starts up glfw context, window and surface.
        vk::raii::su::SurfaceData surfaceData(instance, AppName, vk::Extent2D(window_width, window_height));
        // Get the surface object and find its capabilities...we need the minImageCount later. Note: this is a min, the implementation may
        // force more on you - e.g. if triple buffering were on and you have set minImageCount=2, you can find 3 images in the swapchain.
        vk::raii::SurfaceKHR &rSurface = *surfaceData.pSurface;
        vk::SurfaceCapabilitiesKHR capabilitiesKhr = physicalDevice.getSurfaceCapabilitiesKHR(*rSurface);
        uint32_t imageCount =
                capabilitiesKhr.minImageCount + 1; // Always make room for an extra image...according to docs

        std::pair<uint32_t, uint32_t> graphicsAndPresentQueueFamilyIndex =
                vk::raii::su::findGraphicsAndPresentQueueFamilyIndex(physicalDevice, *surfaceData.pSurface);
        vk::raii::Device device = vk::raii::su::makeDevice(
                physicalDevice, graphicsAndPresentQueueFamilyIndex.first, vk::su::getDeviceExtensions());

        vk::raii::CommandPool commandPool = vk::raii::CommandPool(
                device, {vk::CommandPoolCreateFlagBits::eResetCommandBuffer, graphicsAndPresentQueueFamilyIndex.first});
        vk::raii::CommandBuffer commandBuffer = vk::raii::su::makeCommandBuffer(device, commandPool);
        //NT: Switch to commandbuffers...one for each swapchain image.
        vk::raii::CommandBuffers commandBuffers = makeCommandBuffers(device, commandPool, imageCount);

        vk::raii::Queue graphicsQueue(device, graphicsAndPresentQueueFamilyIndex.first, 0);
        vk::raii::Queue presentQueue(device, graphicsAndPresentQueueFamilyIndex.second, 0);

        // This will set the swapchain size to surfaceCapabilities.minImageCount.
        // NT:: A Swapchain is an array of VkImages. We cannot access these directly. We must use VkImageView wrappers.
        // This creates array of VkImageView, one for each VkImage the device manages.
        vk::raii::su::SwapChainData swapChainData(physicalDevice,
                                                  device,
                                                  *surfaceData.pSurface,
                                                  surfaceData.extent,
                                                  vk::ImageUsageFlagBits::eColorAttachment |
                                                  vk::ImageUsageFlagBits::eTransferSrc,
                                                  {},
                                                  graphicsAndPresentQueueFamilyIndex.first,
                                                  graphicsAndPresentQueueFamilyIndex.second);

        // NT: Allocates 'extent * 1.0 (depth)' of physical device memory to hold the depth buffer.
        // This memory is bound to a VkImage object and then wrapped in a VkImageView object.
        //The range of depths in the depth buffer is 0.0 to 1.0 in Vulkan, where 1.0 lies at the far view plane and 0.0 at the near view plane.
        // The initial value at each point in the depth buffer should be the furthest possible depth, which is 1.0. This is configured when adding
        // RenderPassBegin() command.
        // WARNING: Each Framebuffer created below will hold a reference to this structure - so it must not be deleted prematurely.
        vk::raii::su::DepthBufferData depthBufferData(physicalDevice, device, vk::Format::eD16Unorm,
                                                      surfaceData.extent);

        // ISSUE: BufferData uses a memory allocation per instance - hence no mem sharing between buffers which is inefficient.
        // BufferData() calls Buffer::vkCreateBuffer, check device supports usage and mem flags
        // (note: current util.hpp does not handle failure case), DeviceMemory::vkAllocateMemory, DeviceMemory:: vkBindBufferMemory
        // This uniform buffer lives on the device.
        vk::raii::su::BufferData uniformBufferData(physicalDevice, device, sizeof(glm::mat4x4),
                                                   vk::BufferUsageFlagBits::eTransferDst |
                                                   vk::BufferUsageFlagBits::eUniformBuffer,
                                                   vk::MemoryPropertyFlagBits::eDeviceLocal);

        // Init first PVM matrix and copy to device memory.
        updateUniformBuffer(physicalDevice, surfaceData, device, commandPool, graphicsQueue, uniformBufferData);

        // Repeat for sending time to fragment shader
        vk::raii::su::BufferData uTime(physicalDevice, device, 68 /*sizeof(float)*/,
                                       vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eUniformBuffer,
                                       vk::MemoryPropertyFlagBits::eDeviceLocal);
        updateUTime(physicalDevice, surfaceData, device, commandPool, graphicsQueue, uTime);

        // A descriptor is a pointer into a resource e.g. buffer or image. These resources will be unmarshalled into shader global variables or blocks.
        // A descriptor set is used to group descriptors.
        // A DSL is a schema for a descriptor set. An array of DSLBindings define each descriptor that must be bound
        // to a resource (e.g buffer or image) in the instantiated Descriptor Set.
        // WARNING: No matter the destination stage of a particular descriptor all prior descriptors in that set must be bound (even with dummy data).
        vk::raii::DescriptorSetLayout descriptorSetLayout = vk::raii::su::makeDescriptorSetLayout(
                device, {{vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex},
                         {vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eFragment}});
        vk::raii::PipelineLayout pipelineLayout(device, {{}, *descriptorSetLayout});

        vk::Format colorFormat =
                vk::su::pickSurfaceFormat(physicalDevice.getSurfaceFormatsKHR(**surfaceData.pSurface)).format;
        vk::raii::RenderPass renderPass = vk::raii::su::makeRenderPass(device, colorFormat, depthBufferData.format);

        /* Set up vertex and fragment shader */
        std::string vertexShaderCode = readGLSLFile(
                "C:\\Users\\navee\\CLionProjects\\vulkan_test\\samples\\vulkan-hpp-particles\\shaders\\vertex.vert");
        std::string fragmentShaderCode = readGLSLFile(
                "C:\\Users\\navee\\CLionProjects\\vulkan_test\\samples\\vulkan-hpp-particles\\shaders\\fragment.frag");

        glslang::InitializeProcess(); // Init once per process

        std::vector<unsigned int> vertexShaderSPV;
        bool ok = vk::su::GLSLtoSPV(vk::ShaderStageFlagBits::eVertex, vertexShaderCode, vertexShaderSPV);
        assert(ok);

        // WARNING: We have to provide the size of this spv code in bytes?
        vk::ShaderModuleCreateInfo smciVert = vk::ShaderModuleCreateInfo(
                vk::ShaderModuleCreateFlags(),
                vertexShaderSPV.size() * sizeof(decltype(vertexShaderSPV)::value_type),
                reinterpret_cast<const uint32_t *>(vertexShaderSPV.data()));
        vk::raii::ShaderModule vertexShaderModule = vk::raii::ShaderModule(device, smciVert);

        std::vector<unsigned int> fragmentShaderSPV;
        ok = vk::su::GLSLtoSPV(vk::ShaderStageFlagBits::eFragment, fragmentShaderCode, fragmentShaderSPV);
        assert(ok);

        glslang::FinalizeProcess();
        /* End shader setup */

        vk::ShaderModuleCreateInfo smciFrag = vk::ShaderModuleCreateInfo(
                vk::ShaderModuleCreateFlags(),
                fragmentShaderSPV.size() * sizeof(decltype(fragmentShaderSPV)::value_type),
                reinterpret_cast<const uint32_t *>(fragmentShaderSPV.data()));
        vk::raii::ShaderModule fragmentShaderModule = vk::raii::ShaderModule(device, smciFrag);

        // NT: Create a Frame buffer for each Swapchain VkImage.
        // Post: framebuffers.size() = imageViews.size().
        // Each Framebuffer in the array will contain a reference to two VkImageViews (swapchain VkImage and depth buffer VkImage (created above)).
        // Each Framebuffer will have a reference to the 'depthBufferData' VkImageView we configured above.
        std::vector<vk::raii::Framebuffer> framebuffers = vk::raii::su::makeFramebuffers(
                device, renderPass, swapChainData.imageViews, &*depthBufferData.pImageView, surfaceData.extent);

        // Set up buffer that holds combined array of triangle:line vertices.
        vk::raii::su::BufferData vertexBufferData(
                physicalDevice, device, combinedVertices.size() * sizeof(combinedVertices[0]), //size in bytes of all data in vector.
                vk::BufferUsageFlagBits::eVertexBuffer);
        // Remember: Vk will feed shader one vertex at a time. Shader doesn't know if vertex is part of triangle, square or circle.
        // Hence the stride is only the length of one vertex (x,y,z,w) = 16 bytes.
        vk::raii::su::copyToDevice(
                vertexBufferData.deviceMemory, combinedVertices.data(), // byte buffer of vector
                combinedVertices.size(), //Number of elements in vector
                sizeof(Vertex)); //stride = 16 bytes. Actually done as default value, but is here for reference.

        // Set up buffer that holds indexes for trail line strips and primitive restart symbols between each line.
        vk::raii::su::BufferData indexBufferData(
                physicalDevice, device, indexBuffer.size() * sizeof(indexBuffer[0]), //size in bytes of all data in vector.
                vk::BufferUsageFlagBits::eIndexBuffer);
        vk::raii::su::copyToDevice(
                indexBufferData.deviceMemory, indexBuffer.data(), // byte buffer of vector
                indexBuffer.size(), //Number of elements in vector
                sizeof(indexBuffer[0])); //stride actually done as default value, but is here for reference.

        vk::raii::DescriptorPool descriptorPool =
                vk::raii::su::makeDescriptorPool(device, {{vk::DescriptorType::eUniformBuffer, 2}});
        // NT: A descriptor set is set of resources bound as a group.
        // The structure of each set is defined by a Descriptor layout which describes the order and types of resources in the set.
        // The set of all layouts is called the pipeline layout. Descriptor Sets and Pipelines are created with reference to their layouts.
        // Note: 'the order' of a layout implies each descriptor defined by a layout is mandatory.
        // ISSUE: uses .front() because its only creating a single descriptor set.
        vk::raii::DescriptorSet descriptorSet =
                std::move(vk::raii::DescriptorSets(device, {*descriptorPool, *descriptorSetLayout}).front());
        vk::raii::su::updateDescriptorSets(
                device, descriptorSet, {{vk::DescriptorType::eUniformBuffer, uniformBufferData.buffer, nullptr},
                                        {vk::DescriptorType::eUniformBuffer, uTime.buffer,             nullptr}}, {});

        vk::raii::PipelineCache pipelineCache(device, vk::PipelineCacheCreateInfo());
        // Changes vk default assembly from ePointsList to eTriangleList.
        vk::raii::Pipeline gpTriangleList = vk::raii::su::makeGraphicsPipeline(
                device,
                pipelineCache,
                vertexShaderModule, // Assigns shader to specific shader stage of pipeline (eVertex)
                nullptr,
                fragmentShaderModule, // Assigns shader to specific shader stage of pipeline (eFragment)
                nullptr,
                vk::su::checked_cast<uint32_t>(sizeof(combinedVertices[0])), // Stride for single vertex. Must be >= sum of attribute lengths!
                {{vk::Format::eR32G32B32A32Sfloat, 0}// shader location=0, 4 floats = 32 bytes.
                 },
                vk::FrontFace::eClockwise,
                true,
                pipelineLayout,
                renderPass,
                topTriangleList);

        // NT: We must have a separate graphics pipeline to handle different primitives (triangle list, line list etc.)
        vk::raii::Pipeline gpTrails = vk::raii::su::makeGraphicsPipeline(
                device,
                pipelineCache,
                vertexShaderModule, // Assigns shader to specific shader stage of pipeline (eVertex)
                nullptr,
                fragmentShaderModule, // Assigns shader to specific shader stage of pipeline (eFragment)
                nullptr,
                vk::su::checked_cast<uint32_t>(sizeof(combinedVertices[0])), // Stride for single vertex. Must be >= sum of attribute lengths!
                {{vk::Format::eR32G32B32A32Sfloat, 0}// shader location=0, 4 floats = 32 bytes.
                 },
                vk::FrontFace::eClockwise,
                true,
                pipelineLayout,
                renderPass,
                topTrails,
                topTrails == vk::PrimitiveTopology::eLineStrip ? true: false);//enable primitive restart symbols - so we know when a LineStrip ends and next one starts.
                //todo: move this test to within makeGraphicsPipeline (i.e. true if LineStrip or xStrip is used).

        initCommandBuffers(surfaceData, commandBuffer, pipelineLayout, renderPass, framebuffers,
                           vertexBufferData,
                           indexBufferData,
                           descriptorSet,
                           gpTriangleList,
                           gpTrails,
                           commandBuffers, device, commandPool);

        float testIncr = 0.f;
        GLFWwindow *hWindow = surfaceData.window.handle;
        while (!glfwWindowShouldClose(hWindow)) {
            glfwPollEvents();
            // Copy new PVM matrix to device memory
            updateUniformBuffer(physicalDevice, surfaceData, device, commandPool, graphicsQueue, uniformBufferData,
                                testIncr += 0.01f);
            updateUTime(physicalDevice, surfaceData, device, commandPool, graphicsQueue, uTime);
            // Get the index of the next available swapchain image:
            vk::raii::Semaphore imageAcquiredSemaphore(device, vk::SemaphoreCreateInfo());
            vk::Result result;
            uint32_t imageIndex;
            std::tie(result, imageIndex) =
                    swapChainData.pSwapChain->acquireNextImage(vk::su::FenceTimeout, *imageAcquiredSemaphore);
            assert(result == vk::Result::eSuccess);
            assert(imageIndex < swapChainData.images.size());

            // Send graphics command to the device.
            // Fence is used to signal completion of this set of commands on the GPU.
            vk::raii::Fence drawFence(device, vk::FenceCreateInfo());
            // The final colors are output from the pipeline at the eColorAttachmentOutput stage. Our fence will wait for this event so
            // we know image is ready to be sent to the display.
            vk::PipelineStageFlags waitDestinationStageMask(vk::PipelineStageFlagBits::eColorAttachmentOutput);
            // We trigger the Command 'script' specially written for this Framebuffer.
            vk::SubmitInfo submitInfo(*imageAcquiredSemaphore, waitDestinationStageMask, *commandBuffers[imageIndex]);
            graphicsQueue.submit(submitInfo, *drawFence);

            // Without this you violate the spec: 'All queue submission commands referring to 'fence' must have completed'
            while (vk::Result::eTimeout == device.waitForFences({*drawFence}, VK_TRUE, vk::su::FenceTimeout));

            // Now image in swap chain is ready, we can present it to the user.
            vk::PresentInfoKHR presentInfoKHR(nullptr, **swapChainData.pSwapChain, imageIndex);
            result = presentQueue.presentKHR(presentInfoKHR);
            switch (result) {
                case vk::Result::eSuccess:
                    break;
                case vk::Result::eSuboptimalKHR:
                    std::cout << "vk::Queue::presentKHR returned vk::Result::eSuboptimalKHR !\n";
                    break;
                default:
                    assert(false);  // an unexpected result is returned !
            }

            // Push new vertices after Lorenz update (requires ODEINT so cannot be done by shaders/GPU).
            ps.updateWorldLocations();
            combinedVertices = ps.getAllVertices(); //todo: profiling indicates this is slow (when accessing deque element).
            vk::raii::su::copyToDevice(
                    vertexBufferData.deviceMemory, combinedVertices.data(), // byte buffer of vector
                    combinedVertices.size(), //Number of elements (vertices)
                    sizeof(Vertex)); //stride = 16 bytes. Actually done by default value, but is here for reference.
        }
        device.waitIdle();
    }
    catch (vk::SystemError &err) {
        std::cout << "vk::SystemError: " << err.what() << std::endl;
        exit(-1);
    }
    catch (std::exception &err) {
        std::cout << "std::exception: " << err.what() << std::endl;
        exit(-1);
    }
    catch (...) {
        std::cout << "unknown error\n";
        exit(-1);
    }
    return 0;
}

int main() {
    loadIniFile("particles.ini");

    // We're going to get glfw to use Vulkan
    glfwInit();
    // A window holds a context. In fact the window handle = context handle.
    // A context is created for a particular client API e.g. OpenGL.
    // This context is for 'no api' as we're using Vulkan.
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    // todo: Above code creates its own glfw window...what if we don't want that?
    //GLFWwindow* window = glfwCreateWindow(1920, 1080, "Vulkan example", NULL, NULL);
    // Check for vulkan-1.dll (loader) and an ICD
    if (!glfwVulkanSupported()) {
        std::cout << "Vulkan loader (vulkan-1.dll) or ICD not found" << std::endl;
        return -1;
    }
    // GLFW can list names of Vulkan extensions it needs to draw on Vulkan surfaces.
    uint32_t how_many_extensions_for_glfw = 0;
    const char **glfw_requires_these_extensions = glfwGetRequiredInstanceExtensions(&how_many_extensions_for_glfw);
    std::vector<std::string> exts;
    for (int i = 0; i < how_many_extensions_for_glfw; i++)
        exts.emplace_back(glfw_requires_these_extensions[i]);
    // Note to future self: VK_EXT_extended_dynamic_state is not supported by my Intel driver - so we can't use all dynamic state variables
    // in a graphics pipeline (e.g. dynamic primitive topolgy).

    initVulkanRII(nullptr, exts, how_many_extensions_for_glfw);

    return 1;
}


