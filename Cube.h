//
// Created by navee on 30/06/2021.
//

#ifndef VULKAN_TEST_CUBE_H
#define VULKAN_TEST_CUBE_H


#include "Shape.h"

class Cube : public Shape {
public:
    Cube() { initBuffers(); }

private:
    GLuint getPrimitiveType() override { return GL_TRIANGLES;};
    std::vector<glm::vec3> getLocalCoordsOfVertices() override;
    const GLfloat * getFloatArray() override;
    int getVertexCount() override;
};


#endif //VULKAN_TEST_CUBE_H
