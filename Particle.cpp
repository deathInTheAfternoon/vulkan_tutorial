//
// Created by navee on 02/07/2021.
//

#include "Particle.h"

int noOfVertices = 30;
float radius = 1.f;

std::vector<glm::vec3> Particle::getLocalCoordsOfVertices() {
    static const float pi = 3.141592654f;

    for (int i = 0; i < noOfVertices; i++) {
        float angle = i * 2 * pi / noOfVertices - pi / 2;
        float x = std::cos(angle) * radius;
        float y = std::sin(angle) * radius;
        vertices.push_back(glm::vec3(radius + x, radius + y, 0.f));
    }

    return vertices;
}
