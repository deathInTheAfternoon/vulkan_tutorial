#version 450
layout(location = 0) out vec4 fragColor;

#ifdef GL_ES
precision mediump float;
#endif

#extension GL_OES_standard_derivatives : enable

#define t u_time

layout(set=0, binding=1) uniform stuff {
    float u_time;
};

//uniform vec2 mouse;
vec2 resolution = vec2(1920, 1080);

float tw()
{
    return sin(u_time) * 0.5 + 0.5;
}

vec2 N22(vec2 p)
{
    vec3 a = fract(p.xyx*vec3(123.34, 234.34, 345.65));
    a += dot(a, a+34.45);
    return fract(vec2(a.x*a.y, a.y*a.z));
}

void main(void) {

    vec2 uv = (2.0*gl_FragCoord.xy - resolution.xy)/resolution.y;

    vec3 color = vec3(0.0);
    float m = 0.0;
    float minDist = 1.0;

    for (float i = 0.0; i < 8.0; i++)
    {
        vec2 n = N22(vec2(i)+10.);
        //vec2 n = vec2(0.125*i+0.2, 0.125*i);
        vec2 p = sin(n * t/0.6);

        float d = length(uv - p);
        //m += smoothstep(0.015, 0.01, d);

        minDist = min(d, minDist);
    }

    color = vec3(0.0, 0.0, 0.4 - minDist);
    float tww = 1.1;
    color += step(minDist, tww/3.) * step(tww/3.-0.015, minDist) * vec3(0.0, 1.0, 0.0);
    fragColor = vec4(color, 1.0);

}