//
// Created by navee on 02/07/2021.
//

#ifndef VULKAN_TEST_PARTICLE_H
#define VULKAN_TEST_PARTICLE_H


#include "Shape.h"

class Particle : public Shape {
public:
    Particle() { initBuffers(); }

private:
    GLuint getPrimitiveType() override { return GL_TRIANGLE_FAN; }
    std::vector<glm::vec3> getLocalCoordsOfVertices() override;
    const GLfloat * getFloatArray() override;
    int getVertexCount() override;
    std::vector<glm::vec3> vertices;

};


#endif //VULKAN_TEST_PARTICLE_H
