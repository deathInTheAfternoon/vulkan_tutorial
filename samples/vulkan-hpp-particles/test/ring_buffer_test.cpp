#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include "catch.hpp"
#include <chrono>
#include <random>
#include <vector>
#include <random>
#include "../particle_system/VertexBuffer.hpp"

// Needed by Catch framework REQUIRE(v1 == v2).
bool operator==(const Vertex& lhs, const Vertex& rhs) {
    return (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z);
}

TEST_CASE("Always overwrite oldest then read from next oldest.", "[raw]") {
    GiantBuffer buffer;
    buffer.initialize(1, 2);
    buffer.write(0, {1.f, 1.1f, 1.2f});
    // read[0] should always start from the oldest item...
    Vertex v = buffer.read(0, 0);
    REQUIRE((v.x == 0.f && v.y == 0.f && v.z == 0.f));
    v = buffer.read(0, 1);
    REQUIRE((v.x == 1.f && v.y == 1.1f && v.z == 1.2f));

    // move to next youngest slot
    buffer.write(0, {2.f, 2.1f, 2.2f});
    v = buffer.read(0, 0);
    REQUIRE((v.x == 1.f && v.y == 1.1f && v.z == 1.2f));
    v = buffer.read(0, 1);
    REQUIRE((v.x == 2.f && v.y == 2.1f && v.z == 2.2f));
    // Ring should cycle back to start
    buffer.write(0, {3.f, 3.1f, 3.2f});
    v = buffer.read(0, 0);
    REQUIRE((v.x == 2.f && v.y == 2.1f && v.z == 2.2f));
    v = buffer.read(0, 1);
    REQUIRE((v.x == 3.f && v.y == 3.1f && v.z == 3.2f));

    v = buffer.read(0, 0);
    REQUIRE((v.x == 2.f && v.y == 2.1f && v.z == 2.2f));
    v = buffer.read(0, 1);
    REQUIRE((v.x == 3.f && v.y == 3.1f && v.z == 3.2f));
    v = buffer.read(0, 2);
    REQUIRE((v.x == 2.f && v.y == 2.1f && v.z == 2.2f));
}

TEST_CASE("Extend trail length to 3", "[raw]") {
    GiantBuffer buffer;
    buffer.initialize(1, 3);
    buffer.write(0, {1.f, 1.1f, 1.2f});
    buffer.write(0, {2.f, 2.1f, 2.2f});
    buffer.write(0, {3.f, 3.1f, 3.2f});
    // Overwrite the first item
    buffer.write(0, {4.f, 4.1f, 4.2f});
    // After a write, read(, 0) is from oldest item in current buffer.
    Vertex v = buffer.read(0, 0);
    REQUIRE((v.x == 2.f && v.y == 2.1f && v.z == 2.2f));
}

TEST_CASE("Add a second particle trail", "[raw]") {
    GiantBuffer buffer;
    buffer.initialize(2, 3);
    buffer.write(0, {1.f, 1.1f, 1.2f});
    buffer.write(1, {1.f, 1.1f, 1.2f});
    buffer.write(0, {2.f, 2.1f, 2.2f});
    buffer.write(1, {2.f, 2.1f, 2.2f});
    buffer.write(0, {3.f, 3.1f, 3.2f});
    buffer.write(1, {3.f, 3.1f, 3.2f});
    // Overwrite the first item
    buffer.write(0, {4.f, 4.1f, 4.2f});
    buffer.write(1, {4.f, 4.1f, 4.2f});
    // After a write, read(, 0) is from oldest item in current buffer.
    Vertex v = buffer.read(0, 0);
    REQUIRE((v.x == 2.f && v.y == 2.1f && v.z == 2.2f));
    v = buffer.read(1, 0);
    REQUIRE((v.x == 2.f && v.y == 2.1f && v.z == 2.2f));
}

TEST_CASE("Get linearized array", "[raw]") {
    GiantBuffer buffer;
    buffer.initialize(1, 2);
    buffer.write(0, {1.f, 1.1f, 1.2f});

    std::vector<Vertex>& a = buffer.getOrderedArray();
    Vertex &v = buffer.read(0, 0);
    REQUIRE(v == a[0]);
    v = buffer.read(0, 1);
    REQUIRE(v == a[1]);
}

TEST_CASE("Get linearized array from larger buffer", "[raw]") {
    std::default_random_engine e;
    std::uniform_real_distribution u;

    GiantBuffer buffer;
    buffer.initialize(2, 3);
    buffer.write(0, {1.f, 1.1f, 1.2f});
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 3; j++){
            float some_value = u(e);
            buffer.write(i, {some_value, some_value, some_value});
        }
    }

    std::vector<Vertex>& a = buffer.getOrderedArray();
    Vertex &v = buffer.read(0, 0);
    REQUIRE(v == a[0]);
    v = buffer.read(0, 1);
    REQUIRE(v == a[1]);
    v = buffer.read(0, 2);
    REQUIRE(v == a[2]);
    v = buffer.read(1, 0);
    REQUIRE(v == a[3]);
    v = buffer.read(1, 1);
    REQUIRE(v == a[4]);
    v = buffer.read(1, 2);
    REQUIRE(v == a[5]);
}

TEST_CASE("Get linearized array into pre-allocated buffer.", "[raw]") {
    GiantBuffer buffer;
    buffer.initialize(3, 2);
    buffer.write(0, {1.f, 1.1f, 1.2f});
    buffer.write(0, {2.f, 2.1f, 2.2f});

    std::vector<Vertex> preAllocated(10);
    int offset = 3;
    buffer.fillWithOrderedArray(preAllocated, offset);

    Vertex &v = buffer.read(0, 0);
    REQUIRE(v == preAllocated[offset]);
    v = buffer.read(0, 1);
    REQUIRE(v == preAllocated[offset + 1]);
    v = buffer.read(1, 0);
    REQUIRE(v == preAllocated[offset + 2]);
    v = buffer.read(1, 1);
    REQUIRE(v == preAllocated[offset + 3]);
}

TEST_CASE("Performance check vector and deque", "[.]") {
    // Engine
    std::default_random_engine e;
    std::uniform_real_distribution<float> d(0.f, 20.f);

    const size_t size = 100000;
    const size_t tests = 10000;

    std::vector<float> v(size);
    std::deque<float> dq(size);

    // fill em up
    for (int i = 0; i < size; i++) {
        v[i] = d(e);
        dq[i] = d(e);
    }

    int duration = 0;
    size_t cnt = 0;
    int max = 0;
    for (int i = 0; i < tests; i++, ++cnt) {
        float r = d(e);
        auto t1 = std::chrono::high_resolution_clock::now();

        v.erase(v.begin());
        v.push_back(r);

        auto t2 = std::chrono::high_resolution_clock::now();
        auto delta = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
        duration += delta;

        if (max == 0 || max < delta) {
            max = delta;
        }
    }
    std::cout << "it took an average of " << duration / cnt << "us and a max of " << max << " us" << std::endl;

    duration = 0;
    cnt = 0;
    max = 0;
    for (int i = 0; i < tests; i++, ++cnt) {
        float r = d(e);
        auto t1 = std::chrono::high_resolution_clock::now();

        dq.pop_front();
        dq.emplace_back(r);

        auto t2 = std::chrono::high_resolution_clock::now();
        auto delta = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
        duration += delta;

        if (max == 0 || max < delta) {
            max = delta;
        }
    }
    std::cout << "it took an average of " << duration / cnt << "us and a max of " << max << " us" << std::endl;
}

TEST_CASE("Test vector of deqeues", "[.]") {
    size_t lengthTrail = 100;
    size_t countParticles = 1000;
    size_t sweep = 100; //how many times to sweep through the whole structure.

    typedef std::deque<float> Trail;
    std::vector<Trail> trails;
    trails.resize(countParticles, Trail(lengthTrail));

    // populate
    std::default_random_engine e;
    std::uniform_real_distribution d;
    for (Trail& trail : trails) {
        for (int i = 0; i < trail.size(); i++) {
            trail[i] = d(e);
        }
    }

    // Timing loop for adding new element
    int duration = 0;
    int cnt = 0;
    int max = 0;
    for (int s = 0; s < sweep; s++) {
        for (int i = 0; i < countParticles; i++, ++cnt) {
            float r = d(e);
            auto t1 = std::chrono::high_resolution_clock::now();

            Trail& t = trails[i];
            t.pop_front();
            t.emplace_back(r);

            auto t2 = std::chrono::high_resolution_clock::now();
            auto delta = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
            duration += delta;

            if (max == 0 || max < delta) {
                max = delta;
            }
        }
    }

    std::cout << "it took an average of " << duration / cnt << "E-6 seconds and a max of " << max << " E-6 s" << std::endl;

    // Now stream to a single vector multiple times
    duration = 0;
    cnt = 0;
    max = 0;
    for (int s = 0; s < sweep; s++) {
        auto t1 = std::chrono::high_resolution_clock::now();
        // Pre allocate target buffer
        std::vector<float> buffer(countParticles * lengthTrail);
        // Copy one element at a time?
        int bufferIndex = 0;
        for(int i = 0; i < trails.size(); i++, ++cnt) {
            std::deque<float> trail = trails[i];
            for (int j = 0; j < trail.size(); j++) {
                buffer[bufferIndex++] = trail[j];
            }
        }

        auto t2 = std::chrono::high_resolution_clock::now();
        auto delta = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
        duration += delta;

        if (max == 0 || max < delta) {
            max = delta;
        }
    }
    std::cout << "it took an average of " << duration / cnt << "E-6 seconds and a max of " << max << " E-6 s" << std::endl;
}

TEST_CASE("std::rotate perf test", "[.]") {
    size_t countVertices = 100000;

    std::vector<float> linearBuffer;
    linearBuffer.resize(countVertices);

    std::default_random_engine e;
    std::uniform_real_distribution d;
    // populate 'linearBuffer'...
    for (int i = 0; i < linearBuffer.size(); i++) {
        linearBuffer[i] = d(e);
    }

    int numberOfRotations = 100000;
    int duration = 0;
    int cnt = 0;
    int max = 0;
    for (int i = 0; i < numberOfRotations; i++, ++cnt) {
        auto t1 = std::chrono::high_resolution_clock::now();

        std::rotate(linearBuffer.begin(), linearBuffer.end() - 1, linearBuffer.end());

        auto t2 = std::chrono::high_resolution_clock::now();
        auto delta = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
        duration += delta;

        if (max == 0 || max < delta) {
            max = delta;
        }
    }
    std::cout << "Average time " << duration/cnt << " us, longest time " << max << " us" << std::endl;
}


