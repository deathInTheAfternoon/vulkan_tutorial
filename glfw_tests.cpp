#include <glad/glad.h>
#include "glfw_tests.h"
#include "ShaderManager.h"
#include "Shape.h"
#include "ParticleSystem.h"
#include "Ball.h"
#include <GLFW/glfw3.h>
#include <filesystem>
#include <fstream>
#include <iostream>

namespace glfw_tests {
    int screen_width = 1920, screen_height = 1080;
    float angle = 0.f;
    bool refreshPVM = true;
    float camHeight = 0.f;

    // glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
    void static framebuffer_size_callback(GLFWwindow* window, int width, int height)
    {
        // make sure the viewport matches the new window dimensions; note that width and
        // height will be significantly larger than specified on retina displays.
        glViewport(0, 0, width, height);
    }


    void APIENTRY glDebugOutput(GLenum source,
                                GLenum type,
                                unsigned int id,
                                GLenum severity,
                                GLsizei length,
                                const char *message,
                                const void *userParam)
    {
        // ignore non-significant error/warning codes
        if(id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

        std::cout << "---------------" << std::endl;
        std::cout << "Debug message (" << id << "): " <<  message << std::endl;

        switch (source)
        {
            case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
            case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
            case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
            case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
            case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
            case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
        } std::cout << std::endl;

        switch (type)
        {
            case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
            case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
            case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
            case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
            case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
            case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
            case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
            case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
            case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
        } std::cout << std::endl;

        switch (severity)
        {
            case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
            case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
            case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
            case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
        } std::cout << std::endl;
        std::cout << std::endl;
    }

    // keyboard handling
    static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
    {
        std::cout << key << std::endl;
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
            glfwSetWindowShouldClose(window, GLFW_TRUE);
        if (key == GLFW_KEY_LEFT) angle = angle - 1;
        if (key == GLFW_KEY_RIGHT) angle = angle + 1;
        if (key == GLFW_KEY_UP) camHeight = camHeight + 1;
        if (key == GLFW_KEY_DOWN) camHeight = camHeight - 1;

        refreshPVM = true;

    }


    // Used for projection calculations. TODO: move to shape classes
    float aspectaxis()
    {
        float outputzoom = 1.0f;
        float aspectorigin = 16.0f / 9.0f;
        int aspectconstraint = 1;
        switch (aspectconstraint)
        {
            case 1:
                if ((screen_width / screen_height) < aspectorigin)
                {
                    outputzoom *= (((float)screen_width / screen_height) / aspectorigin);
                }
                else
                {
                    outputzoom *= ((float)aspectorigin / aspectorigin);
                }
                break;
            case 2:
                outputzoom *= (((float)screen_width / screen_height) / aspectorigin);
                break;
            default:
                outputzoom *= ((float)aspectorigin / aspectorigin);
        }
        return outputzoom;
    }

    float recalculatefov()
    {
        return 2.0f * glm::atan(glm::tan(glm::radians(45.0f / 2.0f)) / glfw_tests::aspectaxis());
    }

    int main() {
        if (!glfwInit()) {
            // Init failed
            return -1;
        }
        // Enable debug flag...before create window.
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);

        GLFWwindow *window = glfwCreateWindow(screen_width, screen_height, "GLFW Tests", nullptr, nullptr);
        if (!window) {
            glfwTerminate();
            return -1;
        }
        // OpenGL
        // The context is like a C struct that holds pointers to e.g. a window object (GL_WINDOW_TARGET), a vertex buffer (GL_ARRAY_BUFFER).
        // Binding is the process of setting the matching variable in the context struct which gl funcs will use.
        glfwMakeContextCurrent(window);
        glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
        glfwSetKeyCallback(window, key_callback);

        // OpenGL is ready by this stage so load GLAD thunks
        // Load all OpenGL functions using the glfw loader function
        if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
            std::cout << "Failed to initialize OpenGL context" << std::endl;
            return -1;
        }
        // turn on OpenGL debugging output
        int flags;
        glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
        if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
        {
            std::cout << "Enabling DEBUG!";
            glEnable(GL_DEBUG_OUTPUT);
            glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
            glDebugMessageCallback(glDebugOutput, nullptr);
            glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
        }

        // NDC (-1, 1) mapped to viewport screen coords.
        glViewport(0, 0, 800, 600); // viewport screen-coords: (0,0) at bottom left.

        std::filesystem::path v("..\\shaders\\vertex.glsl");
        std::filesystem::path f("..\\shaders\\fragment.glsl");
        ShaderManager shaderManager;
        shaderManager.buildShaderProgram(v, f);


        //------------------------------
        particles::ParticleSystem<particles::Ball> particles;
        particles.initializeSystem(1000);

        float worldRotationAngleZ = 0.f;

        // Render loop...update positions...draw (which will update TMat).
        while (!glfwWindowShouldClose(window)) {
            glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);

            //worldRotationAngleZ = worldRotationAngleZ + glm::radians(.1f);
            particles.updateWorldLocations();

            refreshPVM = true;
            // Send our shader program to the GPU
            shaderManager.use();

            //glm::mat4 model = glm::rotate(glm::mat4(1.f), glm::radians(worldRotationAngleZ), glm::vec3(0.f, .0f, 1.f));
            glm::mat4 view = glm::lookAt(glm::vec3(0.0, 1.f, 0.0), glm::vec3(0.0, -1.0, -5.f),
                                         glm::vec3(0.0, 1.0, 0.0));
            glm::mat4 projection = glm::perspective(recalculatefov(), 1.0f * screen_width / screen_height, 0.1f,10.0f);
//                glm::mat4 mvp = projection * view * model * local;

            particles.draw(shaderManager, view * glm::mat4(1.f));


            // Push back buffer to front buffer
            glfwSwapBuffers(window);
            // Check for events, update window and run callback.
            glfwPollEvents();
        }

        glfwTerminate();
        return 0;
    }
}
