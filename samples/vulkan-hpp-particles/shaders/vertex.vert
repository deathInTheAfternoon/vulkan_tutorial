#version 450
#extension GL_EXT_debug_printf : enable

// todo: What do these do?
//#extension GL_ARB_separate_shader_objects : enable
//#extension GL_ARB_shading_language_420pack : enable


layout (set = 0, binding = 0) uniform stuff
{
    mat4 mvp;
} uniformBuffer;

layout (location = 0) in vec4 pos;
//layout (location = 1) in vec4 inColor;
vec4 inColor = vec4(1.0, 0.5, 1.0, 1.0);

layout (location = 0) out vec4 outColor;

void main()
{
    //debugPrintfEXT("Pos: %d, %d", pos.x, pos.y);
    outColor = inColor;
    gl_Position = uniformBuffer.mvp * pos;
}