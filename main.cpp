#include "vulkan_tests.h"
#include "glfw_tests.h"
#include "raw_opengl_tests.h"
#include "samples/vulkan-hpp-cube/glfw_vulkan.h"

int main() {
    return vulkan_hpp::main();
}