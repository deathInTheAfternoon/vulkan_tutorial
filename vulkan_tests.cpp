#define GLFW_INCLUDE_VULKAN

#include "vulkan_tests.h"
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <iostream>
#include <stdexcept>
#include <vector>
#include <optional>

namespace vulkan_tests {
    VkInstance instance;
    VkDevice logicalDevice = VK_NULL_HANDLE;
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
    struct QueueFamilyIndices {
        std::optional<uint32_t> graphicsFamily;
        bool isComplete() {
            return graphicsFamily.has_value();
        }
    };
    VkQueue graphicsQueue;

    GLFWwindow* window;
    const uint32_t screen_width = 1920, screen_height = 1080;

    class Shape {
    public:
        void run() {
            initWindow();
            initVulkan();
            mainLoop();
            cleanUp();
        }

    private:
        int initWindow() {
            if (!glfwInit())
                return EXIT_FAILURE;

            glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
            window = glfwCreateWindow(screen_width, screen_height, "NaVulkan", nullptr, nullptr);
        }

        void createInstance() {
            // Vk uses structs instead of function parameters. Weirdly, you often have to set the type of the struct.
            VkApplicationInfo appInfo{}; // optional
            appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO; // set type
            appInfo.pApplicationName = "NaVulkan";
            appInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
            appInfo.pEngineName = "No engine"; // What is an engine?
            appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
            appInfo.apiVersion = VK_API_VERSION_1_2;
            appInfo.pNext = nullptr; //Used for future extensions to this struct.

            // Vk is platform independent API. It has platform dependent extensions. Here glfw will tell us
            // which extensions it needs Vk to enable to draw Window surfaces.
            VkInstanceCreateInfo createInfo{}; //mandatory.
            createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
            createInfo.pApplicationInfo = &appInfo;
            // Glfw will ask for 2 extensions to be enabled...
            uint32_t glfwExtensionCount = 0;
            const char **glfwExtensions;
            glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
            createInfo.enabledExtensionCount = glfwExtensionCount; // 2 extensions
            createInfo.ppEnabledExtensionNames = glfwExtensions; // ("VK_KHR_SURFACE", "VK_KHR_win32_surface")
            createInfo.enabledLayerCount = 0; // Enable global validation layers? No.

            // Vk will hold an 'instance' to represent out application in its world...
            if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS) {
                throw std::runtime_error("failed to create Vk instance");
            }

            // API exercise: list Vk extensions.
            uint32_t extensionCount = 0;
            vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
            std::vector<VkExtensionProperties> extensions(extensionCount);
            vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());
            std::cout << "avaliable extensions: \n";
            for (const auto &extension : extensions) {
                std::cout << '\t' << extension.extensionName << '\n';
            }
        }

        void createLogicalDevice() {
            QueueFamilyIndices indices = findQueueFamilies(physicalDevice);

            VkDeviceQueueCreateInfo queueCreateInfo{};
            queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queueCreateInfo.queueFamilyIndex = indices.graphicsFamily.value();
            queueCreateInfo.queueCount = 1;
            float queuePriority = 1.f;
            queueCreateInfo.pQueuePriorities = &queuePriority;

            VkPhysicalDeviceFeatures deviceFeatures{};

            VkDeviceCreateInfo createInfo{};
            createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
            createInfo.pQueueCreateInfos = &queueCreateInfo;
            createInfo.queueCreateInfoCount = 1;
            createInfo.pEnabledFeatures = &deviceFeatures;
            createInfo.enabledExtensionCount = 0;
            createInfo.enabledLayerCount = 0;
            if (vkCreateDevice(physicalDevice, &createInfo, nullptr, &logicalDevice) != VK_SUCCESS) {
                throw std::runtime_error("failed to create logical device!");
            }
            vkGetDeviceQueue(logicalDevice, indices.graphicsFamily.value(), 0, &graphicsQueue);
        }

        void initVulkan() {
            // Init Vulkan library by creating an instance
            createInstance();
            // Select a GPU card (we only have an integrated Iris!).
            pickPhysicalDevice();
            createLogicalDevice();
        }

        // First pick physical device then construct logical device to interface with that device.
        void pickPhysicalDevice() {
            //First query number of graphics devices
            uint32_t deviceCount = 0;
            vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
            // If no gpus, exit
            if (deviceCount == 0) {
                throw std::runtime_error("Failed to find GPU supporting Vulkan.");
            }
            // Get all device handles and check if they're suitable (TODO: ignoring this part of tutorial for now)
            std::vector<VkPhysicalDevice> devices(deviceCount);
            vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());
            for (const auto& device : devices){
                physicalDevice = device; //there's only one!
            }
        }

        bool isDeviceSuitable(VkPhysicalDevice device) {
            QueueFamilyIndices indices = findQueueFamilies(device);
            return indices.isComplete();
        }

        QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device) {
            QueueFamilyIndices indices;
            uint32_t queueFamilyCount = 0;
            vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
            std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
            vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

            for (int i = 0; i < queueFamilies.size(); i++) {
                if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
                    indices.graphicsFamily = i;
                if (indices.isComplete()) // no need to keep looking
                    break;
            }

            return indices;
        }

        void mainLoop() {
            while (!glfwWindowShouldClose(window)) {
                glfwPollEvents();
            };
        }

        void cleanUp() {
            vkDestroyInstance(instance, nullptr);
            vkDestroyDevice(logicalDevice, nullptr);
            glfwDestroyWindow(window);
            glfwTerminate();
        }

        GLFWwindow *window;
        const uint32_t WIDTH = 800;
        const uint32_t HEIGHT = 600;
        VkInstance instance;
        VkPhysicalDevice physicalDevice = VK_NULL_HANDLE; // handle to physical graphics device.
    };

    int main() {
        Shape shape;
        try {
            shape.run();
        } catch (std::exception &e) {
            std::cerr << e.what() << std::endl;
            return EXIT_FAILURE;
        }
        return EXIT_SUCCESS;
    }
}