//
// Created by navee on 28/06/2021.
//

#include <fstream>
#include <iostream>
#include "ShaderManager.h"
#include <glm/gtc/type_ptr.hpp>

std::string ShaderManager::loadShaderFile(const std::filesystem::path& p) {
    // open file. cwd is 'cmake-build-debug' where .exe lives.
    std::ifstream f;
    try {
        f.open(p);
    } catch (std::system_error &e) {
        std::cerr << e.code().message() << std::endl;
    }
    std::string s = std::string {std::istreambuf_iterator<char>{f}, {}};
    return s;
}

GLuint ShaderManager::compileShader(const std::filesystem::path& p, GLenum type) {
    std::string code = loadShaderFile(p);
    GLuint hShader = glCreateShader(type); // create a vertex shader.
    const char* strCode = code.c_str();
    glShaderSource(hShader, 1, &strCode, nullptr);
    glCompileShader(hShader);
    //check for compile errors
    GLint success;
    GLchar infoLog[512];
    glGetShaderiv(hShader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(hShader, 512, nullptr, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    return hShader;
}

void ShaderManager::linkShadersToProgram(GLuint shaderProgram, GLuint vertexShader, GLuint fragmentShader){
    // link object files
    GLint success;
    GLchar infoLog[512];
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    // check for errors
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glad_glGetProgramInfoLog(shaderProgram, 512, nullptr, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
}

void ShaderManager::buildShaderProgram(const std::filesystem::path &vertexShader, const std::filesystem::path &fragShader) {
    GLuint hVertexShader = compileShader(vertexShader, GL_VERTEX_SHADER);
    GLuint hFragShader = compileShader(fragShader, GL_FRAGMENT_SHADER);
    linkShadersToProgram(hShaderProgram, hVertexShader, hFragShader);
    glDeleteShader(hVertexShader);
    glDeleteShader(hFragShader);
}

void ShaderManager::setUniform(std::string name, glm::mat4 value) {
    GLint indexOfShaderVariable = glGetUniformLocation(hShaderProgram, name.c_str());
    // GL_FALSE means col major not row major.
    glUniformMatrix4fv(indexOfShaderVariable, 1, GL_FALSE, glm::value_ptr(value));

}
