#version 460
layout (location = 0) in vec2 vertexPosLocalSpace;

layout (location = 0) uniform mat4 T;


void main() {
    gl_Position = T * vec4(vertexPosLocalSpace, 0.f, 1.f);
}