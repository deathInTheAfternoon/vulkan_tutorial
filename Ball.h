//
// Created by navee on 02/07/2021.
//

#ifndef VULKAN_TEST_BALL_H
#define VULKAN_TEST_BALL_H


#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "glad/glad.h"

namespace particles {
    class Ball {
    public:
        Ball() { initBuffers(); }
        std::vector<glm::vec3> getLocalCoordsOfVertices();
        virtual int getVertexCount();
        void draw();

        // Overwrite current location
        void setWorldLocation(glm::vec3 newPos);
        // Offset current location
        void patchWorldLocation(glm::vec3 patch);
        glm::vec3 getWorldLocation();

        void setScale(glm::vec3 newScale);
        glm::mat4 getScaleTransformation();

        ~Ball() {
            glDeleteVertexArrays(1, &hVAO);
            glDeleteBuffers(1, &hVBO);
        }

        virtual GLuint getPrimitiveType() { return GL_TRIANGLE_FAN; }


    private:
        GLuint hVAO;
        GLuint hVBO;
        void initBuffers();

        // Position of local-PoT in 3D world
        glm::vec3 worldLocation = glm::vec3(0.f, 0.f, 0.f ); //world coords
        glm::vec3 scale = glm::vec3(1.f, 1.f, 1.f);

        std::vector<glm::vec3> originalLocalPlacement;


    };
}



#endif //VULKAN_TEST_BALL_H
