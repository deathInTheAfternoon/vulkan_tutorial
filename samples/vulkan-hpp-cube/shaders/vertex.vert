#version 450

// todo: What do these do?
//#extension GL_ARB_separate_shader_objects : enable
//#extension GL_ARB_shading_language_420pack : enable


layout (set = 0, binding = 0) uniform stuff
{
    mat4 mvp;
    float u_time; // ISSUE: meant for fragment shader!
} uniformBuffer;

layout (location = 0) in vec4 pos;
layout (location = 1) in vec4 inColor;

layout (location = 0) out vec4 outColor;

void main()
{
    outColor = inColor;
    gl_Position = uniformBuffer.mvp * pos;
}