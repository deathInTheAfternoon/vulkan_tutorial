Vulkan is a low level graphics API (the successor to OpenGL).
It takes a lot of code to get anything done and it doesn't come with primitives such as triangle etc.

GLFW uses OpenGL which is made easier to use by including Glad generated files (see 'include' and 'src' folders). Instructions
for using Glad https://learnopengl.com/Getting-started/Creating-a-window.

How OpenGL binding works:
GLuint buffer;

// Generate a name for a new buffer.
// e.g. buffer = 2
glGenBuffers(1, &buffer);

// Make the new buffer active, creating it if necessary.
// Kind of like:
// if (opengl->buffers[buffer] == null)
//     opengl->buffers[buffer] = new Buffer()
// opengl->current_array_buffer = opengl->buffers[buffer]
glBindBuffer(GL_ARRAY_BUFFER, buffer);

// Upload a bunch of data into the active array buffer
// Kind of like:
// opengl->current_array_buffer->data = new byte[sizeof(points)]
// memcpy(opengl->current_array_buffer->data, points, sizeof(points))
glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

OGL context is kind of a table. The keys are 'targets' such as GL_ARRAY_BUFFER. The values are constants or pointers to OGL memory.
Blocks of allocated memory that hold user supplied data are known as 'buffer objects'.
Other entries in the table e.g. GL_VERTEX_ARRAY are containers that include handles for 'buffer objects'. They hold extra information
that allows programs to know details about what is actually stored in the buffer object.

Shaders don't work
with bit-buffers they need typed data e.g. vertices. So OGL wraps a container object 'vertex array' around a pointer to the bit buffer. Container contains
information describing the contents of the bit buffer (e.g. how big is each vertex and what type e.g. one vertex is 3 floats long).
User data is passed into OGL by copying it into a buffer object.
A buffer object holding vertex data is sometimes called a 'vertex buffer object' in the spec. Remember, it is a typless byte array.
A buffer must be bound to the right entry in the OGL context. The 'target' ( GL_ARRAY_BUFFER, GL_SHADER_STORAGE_BUFFER, GL_QUERY_RESULT_BUFFER) is used
to specify the entry in the OGL context. This is called binding a buffer object to its target.

/// OpenGL setup code before the game loop...
//------------------------------
        // Buffer object: 1 allocate a handle. 2 allocate uninitialized storage. 3 copy user data into buffer.
        // Buffers are owned by OpenGl system. A buffer must be bound to and entry in the OGL context struct to be usable.

        // VAO is a list of lists. Each entry in the first list is called an attribute. You might choose to use the first attribute
        // to store a handle to the underlying VBO (containing a list of vertices for each triangle).
        // You might choose to use the second attribute to hold a list of rgb values for each triangle.

        // Allocate one handle for a new VAO (list of lists) and capture the handle.
        glCreateVertexArrays(1, &VAO);
        // bind the list of lists 'Vertex Array Object' to the OGL context
        glBindVertexArray(VAO);

        // Allocate object buffer handles. These will be used to pass user data to OGL server.
        glGenBuffers(1, &VBO);
        glGenBuffers(1, &EBO);

        // Set the global state table's GL_ARRAY_BUFFER entry.
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        // Now load the buffer object with data. We haven't yet connected the VAO list of attributes to this buffer.
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
        // VAO attribute 0 will describe the contents of (and link to) the object buffer.
        // The VAO list position (first arg) will be used in shader code 'layout(location=0)'.
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
        // Enable this VAO (list of lists) so its used for rendering during calls glDrawArrays, glDrawElements...etc.
        glEnableVertexAttribArray(0);

        // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
        //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
        // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
        glBindVertexArray(0);


SFML build examples: change Workspace settings https://stackoverflow.com/questions/42423536/configure-visual-studio-code-for-cmake-with-some-options to change
set the variables shown here https://www.sfml-dev.org/tutorials/2.5/compile-with-cmake.php.


Vulkan
Surface
Vulkan has extensions for windowing because implementations vary between platforms and because some applications are compute only (e.g. writing images out to disk).

GLSLANG
Download the Debug version of the libs (not the source code) and put the libs in the thirdparty folder and copy the includes
under our include folder. The order of linking to the static libs is very exact, so take note:
target_link_libraries(vulkan-hpp-cube ${GLFW_LIB} ${VULKAN_LIB} utils)
target_link_libraries(vulkan-hpp-cube "C:\\Users\\navee\\CLionProjects\\vulkan_test\\third-party-libs\\glslangd.lib")
target_link_libraries(vulkan-hpp-cube "C:\\Users\\navee\\CLionProjects\\vulkan_test\\third-party-libs\\OGLCompilerd.lib")
target_link_libraries(vulkan-hpp-cube "C:\\Users\\navee\\CLionProjects\\vulkan_test\\third-party-libs\\SPIRVd.lib")
target_link_libraries(vulkan-hpp-cube "C:\\Users\\navee\\CLionProjects\\vulkan_test\\third-party-libs\\OSDependentd.lib")
target_link_libraries(vulkan-hpp-cube "C:\\Users\\navee\\CLionProjects\\vulkan_test\\third-party-libs\\MachineIndependentd.lib")
target_link_libraries(vulkan-hpp-cube "C:\\Users\\navee\\CLionProjects\\vulkan_test\\third-party-libs\\GenericCodeGend.lib")
target_link_libraries(vulkan-hpp-cube "C:\\Users\\navee\\CLionProjects\\vulkan_test\\third-party-libs\\SPIRV-Tools-optd.lib")
target_link_libraries(vulkan-hpp-cube "C:\\Users\\navee\\CLionProjects\\vulkan_test\\third-party-libs\\SPIRV-Toolsd.lib")
target_link_libraries(vulkan-hpp-cube "C:\\Users\\navee\\CLionProjects\\vulkan_test\\third-party-libs\\glslang-default-resource-limitsd.lib")

What extensions does my device support?
To find out ALL about your Vulkan device driver, run 'vulkaninfo --html' command line!!