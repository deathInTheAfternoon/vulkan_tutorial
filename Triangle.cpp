//
// Created by navee on 30/06/2021.
//

#include "Triangle.h"

//todo: convert return type to homogenous coords (i.e. glm::vec4)
// todo: Shader needs to apply transformations, not cpu.
std::vector<glm::vec3> Triangle::getLocalCoordsOfVertices() {

    // position of local origin in world space
    glm::vec4 wPosition(0.f, 0.f, 0.f, 1.f);

    // Local origin of rotation - bottom left vertex of triangle.
    glm::vec4 lRotationOrigin(0.f, 0.f, 0.f, 1.f);


    std::vector<glm::vec4> vertices = std::vector<glm::vec4>(3);
    // These are the anchor points in local space
    vertices[0] = glm::vec4(0.f, 0.f, 0.f, 1.f);
    vertices[1] = glm::vec4(0.5f, 1.f, 0.f, 1.f);
    vertices[2] = glm::vec4(1.f, 0.f, 0.f, 1.f);

    // Pretend it's rotated around local origin
    glm::mat4 id(1.f);
    glm::mat4 xTranslate = glm::translate(id, glm::vec3(1.f, 0.f, 0.f));
    glm::mat4 xRotd = glm::rotate(id, glm::radians(40.f), glm::vec3(0.f, 0.f, 1.0f));
    glm::mat4 xScale = glm::scale(id, glm::vec3(1.f, 2.f, 1.f));
    std::vector<glm::vec3> result;
    for (glm::vec4 vec : vertices) {
        glm::vec4 x = xRotd * xTranslate * vec;
        result.emplace_back(x);
    }

    return result;
}

int Triangle::getVertexCount() {
    return 3;
}
