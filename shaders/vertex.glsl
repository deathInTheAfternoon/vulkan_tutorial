#version 460
layout (location = 0) in vec3 vertexPosLocalSpace;

uniform mat4 worldPlacementAndLocalT;

void main() {
    gl_Position = worldPlacementAndLocalT * vec4(vertexPosLocalSpace.x,
                                                 vertexPosLocalSpace.y,
                                                 vertexPosLocalSpace.z,
                                                 1.0);
}