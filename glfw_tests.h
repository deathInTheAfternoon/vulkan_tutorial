//
// Created by navee on 27/06/2021.
//

#ifndef VULKAN_TEST_GLFW_TESTS_H
#define VULKAN_TEST_GLFW_TESTS_H

namespace glfw_tests {
    int main();
    int testFileSystem();
}

#endif //VULKAN_TEST_GLFW_TESTS_H
