//#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include "catch.hpp"
#include "../particle_system/ParticleSystem.h"

const uint32_t END_MARKER_LINE_STRIP = 0xFFFFFFFF;

TEST_CASE("Check total number of vertices for all particles.") {
    using namespace particles;
    ParticleSystem p;
    int noOfParticles = 1000;
    int verticesPerParticle = 3;
    int trailLength = 100;

    p.initializeSystem(noOfParticles, trailLength);

    REQUIRE(p.getNumberOfParticleVertices() == noOfParticles * verticesPerParticle);
}

TEST_CASE("Check total number of vertices for all trails.") {
    using namespace particles;
    ParticleSystem p;
    int noOfParticles = 1000;
    int trailLength = 100;

    p.initializeSystem(noOfParticles, trailLength);

    REQUIRE(p.getNumberOfTrailVertices() == noOfParticles * trailLength);
}

TEST_CASE("Check byte offset to trail data (i.e. skipping triangle data).") {
    using namespace particles;
    ParticleSystem p;
    int noOfParticles = 1000;
    int trailLength = 100;
    int verticesPerParticle = 3;

    p.initializeSystem(noOfParticles, trailLength);

    REQUIRE(p.getByteOffsetOfTrailData() == noOfParticles * verticesPerParticle * sizeof(Vertex));
}

TEST_CASE("Check total size of returned linear buffer.") {
    using namespace particles;
    ParticleSystem p;
    const int noOfParticles = 1;
    const int verticesPerParticle = 3;
    const int trailLength = 1;

    p.initializeSystem(noOfParticles, trailLength);

    // Get all line strips as linear buffer
    const std::vector<Vertex>& linearBuffer = p.getAllVertices();
    REQUIRE(linearBuffer.size() == noOfParticles * verticesPerParticle + noOfParticles * trailLength);
}

TEST_CASE("Check indexed buffer size.") {
    using namespace particles;
    ParticleSystem p;
    const int noOfParticles = 10;
    const int trailLength = 100;

    p.initializeSystem(noOfParticles, trailLength);

    // Get all line strips as linear buffer
    const std::vector<uint32_t>& indexBuffer = p.getIndexBuffer();
    REQUIRE(indexBuffer.size() == noOfParticles * trailLength + noOfParticles);
}

TEST_CASE("Check indexed buffer contents.") {
    using namespace particles;
    ParticleSystem p;
    int noOfParticles = 1;
    int trailLength = 1;

    p.initializeSystem(noOfParticles, trailLength);

    // Get all line strips as linear buffer
    const std::vector<uint32_t>& indexBuffer = p.getIndexBuffer();
    REQUIRE(indexBuffer[0] == 0);
    REQUIRE(indexBuffer[1] == END_MARKER_LINE_STRIP);

    ParticleSystem p1;
    noOfParticles = 3;
    trailLength = 2;

    p1.initializeSystem(noOfParticles, trailLength);

    // Get all line strips as linear buffer
    const std::vector<uint32_t>& indexBuffer1 = p1.getIndexBuffer();
    REQUIRE(indexBuffer1[2] == END_MARKER_LINE_STRIP);
    REQUIRE(indexBuffer1[5] == END_MARKER_LINE_STRIP);
    REQUIRE(indexBuffer1[8] == END_MARKER_LINE_STRIP);
    // Compare contents
    std::vector<uint32_t> expected{0, 1, END_MARKER_LINE_STRIP, 2, 3, END_MARKER_LINE_STRIP, 4, 5, END_MARKER_LINE_STRIP};
    REQUIRE_THAT(indexBuffer1, Catch::Matchers::Equals(expected));
}