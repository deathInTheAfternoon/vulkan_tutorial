//
// Created by navee on 07/07/2021.
//

#ifndef VULKAN_TEST_VERTICESGENERATOR_H
#define VULKAN_TEST_VERTICESGENERATOR_H

#include <vector>
#include "glad/glad.h"

class VerticesGenerator {
public:
    static std::vector <GLfloat> triangles(int countTriangles);
};


#endif //VULKAN_TEST_VERTICESGENERATOR_H
