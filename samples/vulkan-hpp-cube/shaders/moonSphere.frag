#version 450
layout(location = 0) out vec4 fragColor;

#ifdef GL_ES
precision mediump float;
#endif

#extension GL_OES_standard_derivatives : enable

//uniform float time;
//uniform vec2 mouse;
vec2 resolution = vec2(1920, 1080);

int obj = 0;

vec3 lightDir = normalize(vec3(1.0, 1.0, 1.0));
vec3 ballPos = vec3(0.0, 0.0, 0.0);
float dist(vec3 pos1, vec3 pos2)
{
    return length(pos1 - pos2);
}

struct Vertex
{
    vec3 pos;
    vec3 normal;
};

struct ball
{
    vec3 center;
    float radius;
};

struct Intersect
{
    float t;
    vec3 normal;
    vec3 position;
};

ball ball_ = ball(vec3(0.0,0.0,0.0), 0.5);



float dist_func(vec3 pos, float size)
{
    return length(pos) - size;
}
float det( vec3 a, vec3 b, vec3 c ) {
    return (a.x * b.y * c.z)
    + (a.y * b.z * c.x)
    + (a.z * b.x * c.y)
    - (a.x * b.z * c.y)
    - (a.y * b.x * c.z)
    - (a.z * b.y * c.x);
}

float ball_intersect(vec3 cur, vec3 ray, ball ball)
{
    float a = dot(ray, ray);
    float b = 2.0 * dot(ray, cur - ball.center);
    float c = dot(cur - ball.center,cur - ball.center ) - ball.radius*ball.radius;
    float tmp = b*b - 4.0 * a * c;
    if(tmp<0.0) return -1.0;
    else{
        float t = -b/(2.0*a) - sqrt(tmp);
        if(t > 0.0){
            return t;
        }
        else{
            return -1.0;
        }
    }
}
float plane_intersect(vec3 cur, vec3 ray, ball ball)
{
    vec3 p = vec3(0.0, 0.0, 0.0);
    vec3 n = normalize(vec3(0.0,0.0,1.0));
    return 0.0;

}
void triangle_intersect(vec3 cur, vec3 ray, vec3 a_, vec3 b_, vec3 c_, inout Intersect nearest)
{
    float det_div = det(a_ - b_, a_ - c_, ray);
    float det_t = det(a_ - b_, a_ - c_, a_ - cur);
    float det_beta = det(a_ - b_, a_ - cur, ray);
    float det_gamma = det(a_ - cur, a_ - c_, ray);
    nearest.t = -1.0;
    if(det_div == 0.0) return;
    if(det_t/det_div < 0.0 || det_beta/det_div < 0.0 || det_gamma/det_div < 0.0 || 1.0 < det_beta/det_div || 1.0 < det_gamma/det_div || 1.0 < det_gamma/det_div + det_beta/det_div)
    {
        nearest.t = -1.0;
        return;
    }
    else{
        nearest.t = det_t/det_div;
        nearest.normal = cross(b_ - a_, b_ - c_);
        return;
    }
}

vec3 BallNormal(vec3 center, vec3 surface)
{
    return normalize(surface - center);
}

void main( void )
{
    vec2 pos = (gl_FragCoord.xy * 2.0 - resolution.xy) / min(resolution.x, resolution.y);

    vec3 col = vec3(0.0);

    vec3 cameraPos = vec3(0.0, 0.0, 10.0);

    vec3 ray = normalize(vec3(pos, 0.0) - cameraPos);
    vec3 cur = cameraPos;
    float size = 0.5;
    float t;
    if(obj == 0)
    {
        t = ball_intersect(cur, ray, ball_);
        if(t >= 0.0){
            cur += ray * t;
            vec3 normal = BallNormal(ball_.center, cur);
            float diff = dot(normal, lightDir);
            col = vec3(diff) + vec3(0.1);
        }

    }
    else if(obj == 1)
    {
        Intersect n = Intersect(-1.0, vec3(0.0), vec3(0.0));
        triangle_intersect(cur, ray, vec3(0.0,0.0,2.0) , vec3(0.0,0.2, 0.0), vec3 (0.2,0.0 ,0.0), n);
        t = n.t;
        float diff = dot(n.normal, lightDir);
        col = vec3(diff) + vec3(0.1);
    }

    fragColor = vec4(col, 1.0);
}