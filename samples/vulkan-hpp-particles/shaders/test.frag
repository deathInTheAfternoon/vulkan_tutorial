#version 450

#extension GL_EXT_debug_printf : enable

//[opt-layout-qualifier][storage-qualifier][type][var-name]
// Storage-qualifier: in. Type: vec4
layout (location = 0) in vec4 color;
// Storage-qualifier: out. Type. vec4.
layout (location = 0) out vec4 outColor;

// Interface-block: [opt-layout-qualifier] [interface-qualifier] [block-name] {[member-list]}
layout(set=0, binding=1) uniform stuff {
    float u_time;
};

void main() {
    outColor = color;
    outColor = vec4(abs(sin(u_time)), abs(cos(u_time)), 0.0, 1.0);
    debugPrintfEXT("u_time: %f \n", u_time);
}