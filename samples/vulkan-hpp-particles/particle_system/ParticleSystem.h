//
// Created by navee on 04/07/2021.
//

#ifndef VULKAN_TEST_PARTICLESYSTEM_H
#define VULKAN_TEST_PARTICLESYSTEM_H

#include <vector>
#include <random>
#include <glm/gtc/matrix_transform.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>
#include <deque>
#include "VertexBuffer.hpp"

namespace particles {
    using namespace std;

    // todo: enable/disable primitive restart. This adds a 'restart smbol' after each line strip so Vk knows its ended. If not using line strip, do NOT add this symbol.
    class ParticleSystem {
        typedef boost::array< double , 3 > state_type; // initial value when solving ODE.
        typedef std::deque<Vertex> Trail;
    public:
        // TODO: Performance???
        const std::vector<Vertex>& getAllVertices() {
            // Copy trail vertices into combined buffer after triangle data.
            int offset = countParticles * verticesPerParticle;
            giantBuffer.fillWithOrderedArray(combinedBuffer, offset);
            return combinedBuffer;
        }

        void addToHistory(int particleIndex, const Vertex& newPos) {
            if (fuse == 0) {
                fuse = trailSampleRate;
                giantBuffer.write(particleIndex, newPos);
            } else {
                fuse--;
            }
        }

        void initializeSystem(int particleCount, int trailLength, int sampleRate) {
            trailSampleRate = sampleRate;
            countParticles = particleCount;
            verticesPerTrail = trailLength;

            // Each particle has a SINGLE matching Trail whose coords are captured from triangle node 0.
            trails.resize(countParticles, Trail(verticesPerTrail));
            giantBuffer.initialize(countParticles, verticesPerTrail);

            // Index buffer has one index per vertex + 'primitive restart' at end of each line strip (one line strip per particle).
            indexBuffer.resize(countParticles * verticesPerTrail + countParticles);
            // fill in pointers and 'primitive restart' symbols - this will never change.
            int j = 0; //index to vertex array
            int k = verticesPerTrail; // add marker every k elements.
            for (int i = 0; i < indexBuffer.size(); i++) {
                if (k == 0) {
                    indexBuffer[i] = END_MARKER_LINE_STRIP;
                    k = verticesPerTrail;
                } else {
                    indexBuffer[i] = j;
                    j++;
                    k--;
                }
            }


            std::random_device device;
            std::mt19937 generator(device());
            std::uniform_real_distribution<float> distribution(-5.f, 5.f);

            int particleIndex = 0;//used to add vertex to particle's trail
            // Keep triangles in final buffer, we'll copy in trails each time buffer is requested.
            combinedBuffer.resize((countParticles * verticesPerParticle) + (countParticles * verticesPerTrail));
            for (vector<Vertex>::iterator it = combinedBuffer.begin(); it != combinedBuffer.begin() + (countParticles * verticesPerParticle); it++) {
                Vertex& vertex1 = *it;
                it++;
                Vertex& vertex2 = *it;
                it++;
                Vertex& vertex3 = *it;
                vertex1.x = distribution(generator);
                vertex1.y = distribution(generator);
                vertex1.z = 0.0f;// distribution(generator);
                vertex2.x = vertex1.x + width/2.f; // Important: order vertices Clock Wise.
                vertex2.y = vertex1.y + height;
                vertex2.z = vertex1.z;
                vertex3.x = vertex1.x + width;
                vertex3.y = vertex1.y;
                vertex3.z = vertex1.z;

                addToHistory(particleIndex, vertex1);
                particleIndex++;
            }
        };

        void updateWorldLocations() {
            vector<Vertex>::iterator it;
            int particleIndex = 0; //used to select trail
            // Iterator stops at end of triangle data (actually at start of last triangle vertex hence the '- 3'). Beyond that will be line strip data.
            for (it = combinedBuffer.begin(); it <= combinedBuffer.begin() + ((countParticles * verticesPerParticle) - 3); it++) {
                Vertex& vertex1 = *it;
                it++;
                Vertex& vertex2 = *it;
                it++;
                Vertex& vertex3 = *it;

                boost::numeric::odeint::runge_kutta4<state_type> stepper;
                state_type x = { vertex1.x, vertex1.y, vertex1.z}; // initial conditions
                state_type out;

                stepper.do_step(lorenz, x, t, out, dt);
                // WARNING: triangle nodes are drawn CW.
                vertex1.x = out[0];
                vertex1.y = out[1];
                vertex1.z = out[2];
                vertex2.x = vertex1.x + width/2.f;
                vertex2.y = vertex1.y + height;
                vertex2.z = vertex1.z;
                vertex3.x = vertex1.x + width;
                vertex3.y = vertex1.y;
                vertex3.z = vertex1.z;

                addToHistory(particleIndex, vertex1);
                particleIndex++;
            }
        };

        static void lorenz( const state_type &x , state_type &dxdt , double t )
        {
            const double sigma = 10.0;
            const double R = 30.0;
            const double b = 8.0 / 3.0;

            dxdt[0] = sigma * ( x[1] - x[0] );
            dxdt[1] = R * x[0] - x[1] - x[0] * x[2];
            dxdt[2] = -b * x[2] + x[0] * x[1];
        };

        int getNumberOfParticleVertices() {
            return countParticles * verticesPerParticle;
        }

        // Number of vertices in a single trail.
        int getNumberOfTrailVertices(){
            return countParticles * verticesPerTrail;
        }

        const vector<Vertex>& getTrailVertices() {
            int pos = 0;
            for (int i = 0; i < trails.size(); i++) {
                const Trail& trail = trails[i];
                for (int j = 0; j < trail.size(); j++, pos++) {
                    combinedBuffer[pos] = trail[j]; //todo: avoid copy; use reference semantics?
                }
            }
            return combinedBuffer;
        }

        uint64_t getByteOffsetOfTrailData() {
            return countParticles * verticesPerParticle * sizeof(Vertex);
        }

        const vector <uint32_t> &getIndexBuffer() {
            return indexBuffer;
        }

        const uint32_t getIndexBufferSize() {
            return indexBuffer.size();
        }

    private:
        //One trail queue per particle. Must initialize with zero Vertex so indexBuffer has something to map to.
        std::vector<Trail> trails;
        // Must use indexed buffer to get line strips to work. NOTE, never updated once created.
        std::vector<uint32_t> indexBuffer;
        // All data packed into a single buffer
        std::vector<Vertex> combinedBuffer;
        GiantBuffer giantBuffer;

        int countParticles;
        int verticesPerParticle = 3;
        int verticesPerTrail = 1;
        const uint32_t END_MARKER_LINE_STRIP = 0xFFFFFFFF;
        int trailSampleRate; // only add history every n'th vertex
        int fuse = 0; // MUST initialize to zero thus ensuring we capture first pos of particle.
        // Particle triangle
        float width = 0.25f, height = 0.25f;
        // ODE parameters
        float dt = 0.005;//0.0005371; // animation time step todo: switch to real-time?
        float t = 0.f; //start time for simulation
    };
}
#endif //VULKAN_TEST_PARTICLESYSTEM_H
