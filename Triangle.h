//
// Created by navee on 30/06/2021.
//

#ifndef VULKAN_TEST_TRIANGLE_H
#define VULKAN_TEST_TRIANGLE_H


#include "Shape.h"

class Triangle : public Shape {
public:
    // initBuffers is defined in shape but calls virtual function getLocalCoords()...not allowed from a ctor.
    Triangle() { initBuffers(); }
private:
    std::vector<glm::vec3> getLocalCoordsOfVertices() override;
    int getVertexCount() override;
    GLuint getPrimitiveType() override { return GL_TRIANGLES;}
};


#endif //VULKAN_TEST_TRIANGLE_H
