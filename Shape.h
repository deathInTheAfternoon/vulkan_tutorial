//
// Created by navee on 28/06/2021.
//

#ifndef VULKAN_TEST_SHAPE_H
#define VULKAN_TEST_SHAPE_H
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include <glad/glad.h>
#include <iostream>

// The 'origin' of the LCS is always (0, 0, 0).
// The 'first vertex' of our triangle is always the bottom left vertex.
// The 'first vertex' is positioned wrt the LCS origin. By changing the 'first vertex pos',
// we could align the top right vertex, the triangle's center or even the first vertex itself
// with the LCS origin.
class Shape {
public:
    void initBuffers();
    virtual std::vector<glm::vec3> getLocalCoordsOfVertices() = 0;
    virtual const GLfloat* getFloatArray() { return nullptr; }
    virtual int getVertexCount() = 0;

    void draw();

    glm::mat4 getTransformationMatrix() {
        // 'Translate' local coord origin to get its world location.
        glm::mat4 translationMatrix = glm::mat4(1.f);
        translationMatrix = glm::translate(glm::mat4(1.f), glm::vec3());
        return glm::mat4();
    }

    ~Shape() {
        glDeleteVertexArrays(1, &hVAO);
        glDeleteBuffers(1, &hVBO);
    }

protected:
    virtual GLuint getPrimitiveType() = 0;


private:
    GLuint hVAO;
    GLuint hVBO;


    // TEST
//    GLfloat vertices[12] = {
//            0.5f,  0.5f, 0.0f,  // top right
//            0.5f, -0.5f, 0.0f,  // bottom right
//            -0.5f, -0.5f, 0.0f,  // bottom left
//            -0.5f,  0.5f, 0.0f   // top left
//    };
};


#endif //VULKAN_TEST_SHAPE_H
